import React from "react"
import ReactDOM from "react-dom"
import App from "./App"
import { Provider } from "react-redux"
import { applyMiddleware, createStore, combineReducers } from "redux"
import createHistory from "history/createBrowserHistory"
import rootReducer from "./reducers"
import {
  ConnectedRouter,
  routerReducer,
  routerMiddleware
} from "react-router-redux"
import { reducer as formReducer } from "redux-form"
import thunk from "redux-thunk"

const history = createHistory()
const routerMiddle = routerMiddleware(history)

const store = createStore(
  combineReducers({
    app: rootReducer,
    router: routerReducer,
    form: formReducer
  }),
  applyMiddleware(
    thunk,
    routerMiddle,
  )
)

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root")
)
