import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { setUser } from "../actions"
import { isAuthenticated, getAuth } from "../helper"


const AuthorizedRoute = ({ component: Component, isLoggedIn, setUser, ...rest }) => {
    let authenticated = false
    if (!isLoggedIn && isAuthenticated()) {
      const { userid } = getAuth()
      setUser(userid)
      authenticated = true
    }
    return(

        <Route
            {...rest}
            render={props =>
                isLoggedIn || authenticated ? (
                    <Component {...props} />
                ) : (
                        <Redirect to={{pathname: "/login"}}/>
                    )
            }
        />
    );
}

function mapStateToProps({app}){
    return{
        isLoggedIn: app.isLoggedIn
    }
}

export default connect(mapStateToProps, { setUser })(AuthorizedRoute)