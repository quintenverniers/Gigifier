import React from "react"
import { connect } from 'react-redux'
import { toggleTransition } from "../actions"

class PageLoader extends React.Component {
  componentDidMount() {
    setTimeout(() => this.props.toggleTransition(), 1000)
  }

  render() {
    return (
      <div className="page-loading">
        <img src="/images/loader.gif" alt="Loader" />
      </div>
    )
  }
}

export default connect(
  null,
  { toggleTransition }
)(PageLoader)
