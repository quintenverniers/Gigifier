import React from "react"
import moment from 'moment'
const GigItem = ({
  title = "TBA",
  date = "TBA",
  location = "TBA",
}) => {
  return (
    <div className="edu-history style2">
      <i />
      <div className="edu-hisinfo">
        <h3>{title}</h3>
        <i>{moment(date).format("MMMM D, YYYY HH:mm A")}</i>
        <p>{location}</p>
      </div>
    </div>
  )
}

export default GigItem
