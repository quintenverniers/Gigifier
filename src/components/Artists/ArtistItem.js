import React from "react"
import { connect } from "react-redux"
import { Link } from "react-router-dom"

const ArtistTypes = [
  { value: "solo", label: "Solo Artist" },
  {
    value: "band",
    label: "Band"
  },
  {
    value: "instrument",
    label: "Instrument Player"
  },
  { value: "orchestra", label: "Orchestra" }
  ]

const ArtistItem = ({
  id,
  name = "bogart",
  type = "artist",
  location = "No Location",
  image = null
}) => {
  return (
    <Link className="emply-resume-list round" to={`/profile/${id}`}>
      <div className="emply-resume-thumb">
        <img src={image ? `https://gigifier.com/server${image}` : "/images/portrait.png"} alt="" />
      </div>
      <div className="emply-resume-info">
        <h3>
          <span>{name}</span>
        </h3>
        <span>
          <i>{ArtistTypes.find(atype=>atype.value === type ).label}</i>
        </span>
        <p>
          <i className="la la-map-marker" />
          {location}
        </p>
      </div>
      {/* {isLoggedIn && (
        <div className="shortlists">
          <span title="">
            Shortlist <i className="la la-plus" />
          </span>
        </div>
      )} */}
    </Link>
  )
}

function mapStateToProps({ app }) {
  return {
    isLoggedIn: app.isLoggedIn
  }
}

export default connect(mapStateToProps)(ArtistItem)
