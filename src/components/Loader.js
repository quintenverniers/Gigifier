import React from "react"
import Loader from "react-loader-spinner"
import Modal from "react-modal"

const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      zIndex: 10
    }
  }

const AppLoader = ({loading}) => {
  return (
    <Modal isOpen={loading} style={customStyles} ariaHideApp={false}>
      <Loader type="Bars" color="#7647a2" height="100" width="100" />
    </Modal>
  )
}

export default AppLoader
