import React from "react"
import { connect } from "react-redux"
import { Link } from "react-router-dom"

const EventItem = ({
  id,
  image = "https://placehold.it/235x115",
  title = "Event",
  email = "Artist",
  location = "Location",
  isLoggedIn
}) => {
  return (
    <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <Link to={`/event/${id}`}>
        <div className="job-grid border">
          <div className="job-title-sec">
            <div className="c-logo">
              <img src={image} alt="" />
            </div>
            <h3>
              <span>{title}</span>
            </h3>
            {/* <span>{artist}</span> */}
            <span className="fav-job">
              <i className="la la-heart-o" />
            </span>
          </div>
          <h4 className="job-lctn">{location}</h4>
          {isLoggedIn && <span title=""><a href={`mailto:${email}`}>APPLY NOW</a></span>}
        </div>
      </Link>
    </div>
  )
}

function mapStateToProps({ app }) {
  return {
    isLoggedIn: app.isLoggedIn
  }
}

export default connect(mapStateToProps)(EventItem)
