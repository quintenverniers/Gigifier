import React from 'react'
import { Field, reduxForm } from 'redux-form'
import FileInput from './fileInput'

let LoginForm = props => {
    const { handleSubmit } = props
    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label htmlFor="username">Username</label>
                <Field name="username" component="input" type="text" />
            </div>
            <div>
                <label htmlFor="lastName">Password</label>
                <Field name="lastName" component="input" type="password" />
            </div>
            <div>
                <label htmlFor="test">test</label>
                <Field name="test" component={FileInput} type="file" />
            </div>
            <button type="submit">Submit</button>
        </form>
    )
}

LoginForm = reduxForm({
    form: 'login'
})(LoginForm)

export default LoginForm