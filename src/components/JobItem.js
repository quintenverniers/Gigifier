import React from "react"
import { getLocationName } from "../helper"
import moment from "moment"
import { Link } from 'react-router-dom'

class JobItem extends React.Component {
  render() {
    const { eventName, eventCity, eventCountry, eventCreated, id } = this.props
    return (
      <tr>
        <td>
          <div className="table-list-title">
            <h3>
              <a href="#" title="">
                {eventName}
              </a>
            </h3>
            <span>
              <i className="la la-map-marker" />
              {getLocationName(eventCountry, eventCity)}
            </span>
          </div>
        </td>
        {/* <td>
          <span className="applied-field">3+ Applied</span>
        </td> */}
        <td>
          <span>{moment(eventCreated).format("MMM D, YYYY")}</span>
          {/* <br /> */}
          {/* <span>April 25, 2011</span> */}
        </td>
        {/* <td>
          <span className="status active">Active</span>
        </td> */}
        <td>
          <ul className="action_job">
            <li>
              <span>View Job</span>
              <Link to={`/event/${id}`}>
                <i className="la la-eye" />
              </Link>
            </li>
            {/* <li>
              <span>Edit</span>
              <a href="#" title="">
                <i className="la la-pencil" />
              </a>
            </li> */}
            {/* <li>
              <span>Delete</span>
              <a href="#" title="">
                <i className="la la-trash-o" />
              </a>
            </li> */}
          </ul>
        </td>
      </tr>
    )
  }
}

export default JobItem
