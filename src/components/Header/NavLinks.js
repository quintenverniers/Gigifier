import React from "react"
import { Link } from "react-router-dom"

export default function NavLinks(props) {
  return (
    <ul>
        <li>
          <Link to="/" title="Home">
            Home
          </Link>
        </li>
        <li>
          <Link to="/contactus" title="Contact Us">
            Contact Us
          </Link>
        </li>
      {props.showSearch && (
        <React.Fragment>
          <li>
            <Link to="/search/events" title="Find Gigs">
              Find Gigs
            </Link>
          </li>
          <li>
            <Link to="/search/artists" title="Find Artists">
              Find Artists
            </Link>
          </li>
        </React.Fragment>
      )}
      {/* <li>
        <Link to="/" title="How it Works">
          How it Works
        </Link>
      </li> */}
      {/* <li className="menu-item-has-children">
        <Link to="/" title="About Us">
          About Us
        </Link>
        <ul>
          <li>
            <Link to="/"> Sample Sub Page</Link>
          </li>
          <li>
            <Link to="/">Sample Sub Page</Link>
          </li>
        </ul>
      </li> */}
    </ul>
  )
}
