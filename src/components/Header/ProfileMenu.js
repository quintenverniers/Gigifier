import React, { Component } from "react"
import { Link, Redirect } from "react-router-dom"
import { connect } from "react-redux"
import { setName, setImage } from "../../actions"
import { push } from "react-router-redux"
import { withRouter } from "react-router"

class ProfileMenu extends Component {
  state = {
    profPicUrl: "",
    redirect: false
  }

  componentDidMount() {
    console.log(this.props)
    window.$("body").on("click", function() {
      window.$(".menuitem").fadeOut()
    })
    window.$(".btns-profiles-sec > span").on("click", function() {
      window.$(".menuitem").fadeToggle()
    })
    window.$(".btns-profiles-sec").on("click", function(e) {
      e.stopPropagation()
    })

    if (!this.props.name) {
    }
    if (
      this.props.redirect ||
      (!this.props.redirect &&
        this.props.match.path === "/password" &&
        !this.props.name)
    ) {
      this.props.push("/editprofile?mandatory=true")
    }
  }

  componentWillReceiveProps(nextProps) {
    console.log(
      nextProps.location.path,
      this.props.location.path,
      this.props.redirect
    )
    if (nextProps.match.path !== this.props.match.path && this.props.redirect) {
      this.props.push("/editprofile?mandatory=true")
    }
  }

  render() {
    var loggedIn = false;
    const image = this.props.image
      ? `https://gigifier.com/server${this.props.image}`
      : "/images/portrait.png"
    if(loggedIn){
      return (
        <div className="btns-profiles-sec">
          <span>
            <img src={image} width="50px" height="50px" alt="profile" />{" "}
            {this.props.name}
            <i className="la la-angle-down" />
          </span>
          <ul className="menuitem">
            {!this.props.isArtist && (
              <li>
                <Link to="/manage">
                  <i className="la la-briefcase" /> Manage Postings
                </Link>
              </li>
            )}
            {!this.props.isArtist && (
              <li>
                <Link to="/job">
                  <i className="la la-briefcase" /> Post a New Job
                </Link>
              </li>
            )}
            {this.props.isArtist && (
              <React.Fragment>
                <li>
                  <Link to={`/profile/${this.props.userid}`}>
                    <i className="la la-book" /> My Profile
                  </Link>
                </li>
                <li>
                  <Link to="/editprofile">
                    <i className="la la-leaf" /> Edit Profile
                  </Link>
                </li>
              </React.Fragment>
            )}
            <li>
              <Link to="/password">
                <i className="la la-key" /> Change Password
              </Link>
            </li>
            <li>
              <Link to="/userlogout">
                <i className="la la-history" /> Logout
              </Link>
            </li>
          </ul>
        </div>
      )
    } else {
      return (
        <div className="btns-profiles-sec">
          <span>
              <Link to="/manage">
                <i className="la la-external-link-square" /> Login
              </Link>
          </span>
        </div>
      )
    }
  }
}

function mapStateToProps({ app }) {
  return {
    userid: app.userid,
    name: app.name,
    image: app.image,
    isArtist: app.isArtist,
    redirect: app.redirect
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    { setName, setImage, push }
  )(ProfileMenu)
)
