import React from "react"

function JobListing() {
  return (
    <li>
      <div className="job-listing">
        <div className="job-title-sec">
          <div className="c-logo">
            {" "}
            <img src="https://placehold.it/98x51" alt="" />{" "}
          </div>
          <h3>
            <a title="">Solo Artist</a>
          </h3>
          <span>Massimo Artemisis</span>
        </div>
      </div>
    </li>
  )
}

export default JobListing
