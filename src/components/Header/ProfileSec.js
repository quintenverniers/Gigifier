import React from 'react'

export default function ProfileSec({name = "Chvrches"}){
    return(
        <div className="my-profiles-sec">
        <span>
          <img src="https://placehold.it/50x50" alt="" /> {name}
          <i className="la la-bars" />
        </span>
      </div>
    )
}