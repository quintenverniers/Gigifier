import React, { Component } from "react"
import Routes from "./Routes"
import { isAuthenticated, getAuth } from "./helper"
import { connect } from "react-redux"
import { setUser, toggleLoading } from "./actions"
import { withRouter } from "react-router-dom"
import "react-toastify/dist/ReactToastify.css"

class App extends Component {
  componentDidMount() {
    const { isLoggedIn, setUser } = this.props
    if (!isLoggedIn && isAuthenticated()) {
      const { userid } = getAuth()
      setUser(userid)
    }
  }

  render() {
    return <Routes/>
  }
}

function mapStateToProps({ app }) {
  const { isLoggedIn, isTransitioning } = app
  return {
    isLoggedIn,
    isTransitioning
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    { setUser, toggleLoading }
  )(App)
)
