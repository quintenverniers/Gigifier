import {
  LOGIN_SUCCESSFUL,
  LOGOUT_SUCCESSFUL,
  TOGGLE_TRANSITION,
  SET_USER,
  SET_NAME,
  SET_IMAGE,
  SET_NAME_ONLY,
  SET_REDIRECT
} from "../actions"

const initialState = {
  name: null,
  isLoggedIn: false,
  userid: null,
  isTransitioning: true,
  image: "",
  isArtist: null,
  email: null,
  redirect: false
}

export default function app(state, action) {
  if (typeof state === "undefined") {
    return initialState
  }

  switch (action.type) {
    case LOGIN_SUCCESSFUL:
      return {
        ...state,
        isLoggedIn: false, //permantly set to false
        userid: action.userid,
        isTransitioning: false
      }
    case LOGOUT_SUCCESSFUL:
      return {
        ...state,
        isLoggedIn: false,
        user: null
      }
    case TOGGLE_TRANSITION:
      return {
        ...state,
        isTransitioning: !state.isTransitioning
      }
    case SET_USER:
      return {
        ...state,
        userid: action.userid,
        isLoggedIn: false, //permantly set to false
        isTransitioning: false
      }
    case SET_IMAGE:
      return {
        ...state,
        image: action.image
      }
    case SET_NAME:
      return {
        ...state,
        name: action.name,
        isArtist: action.isArtist,
        email: action.email,
        profilepic: action.profilepic,
        redirect: action.redirect
      }
    case SET_NAME_ONLY:
      return {
        ...state,
        name: action.name
      }
    case SET_REDIRECT:
      return {
        ...state,
        redirect: action.redirect
      }
    default:
      return state
  }
}
