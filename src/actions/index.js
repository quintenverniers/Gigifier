export const LOGIN = "LOGIN"
export const LOGIN_SUCCESSFUL = "LOGIN_SUCCESSFUL"
export const LOGIN_FAILED = "LOGIN_FAILED"
export const LOGOUT = "LOGOUT"
export const LOGOUT_SUCCESSFUL = "LOGOUT_SUCCESSFUL"
export const LOGOUT_FAILED = "LOGOUT_FAILED"
export const TOGGLE_LOADING = "TOGGLE_LOADING"
export const TOGGLE_TRANSITION = "TOGGLE_TRANSITION"
export const SET_USER = "SET_USER"
export const SET_NAME = "SET_NAME"
export const SET_IMAGE = "SET_IMAGE"
export const SET_NAME_ONLY = "SET_NAME_ONLY"
export const SET_REDIRECT = "SET_REDIRECT"

export function loginSuccessful(userid) {
  return {
    type: LOGIN_SUCCESSFUL,
    userid
  }
}

export function loginFailed() {
  return {
    type: LOGIN_FAILED
  }
}

export function logoutSuccessful() {
  return {
    type: LOGOUT_SUCCESSFUL
  }
}

export function logoutFailed() {
  return {
    type: LOGOUT_FAILED
  }
}

export function toggleLoading() {
  return {
    type: TOGGLE_LOADING
  }
}

export function setUser(userid) {
  return {
    type: SET_USER,
    userid
  }
}

export function toggleTransition() {
  return {
    type: TOGGLE_TRANSITION
  }
}

export function setName(name, isArtist, email, profilepic, redirect) {
  return {
    type: SET_NAME,
    name,
    isArtist, email, profilepic, redirect
  }
}

export function setImage(image) {
  return {
    type: SET_IMAGE,
    image
  }
}

export function setNameOnly(name){
  return {
    type: SET_NAME_ONLY,
    name
  }
}

export function setRedirect(redirect){
  return {
    type: SET_REDIRECT,
    redirect
  }
}