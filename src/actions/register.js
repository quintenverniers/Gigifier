

export const REGISTER = "REGISTER";
export const REGISTER_SUCCESSFUL = "REGISTER_SUCCESSFUL";
export const REGISTER_FAILED = "REGISTER_FAILED";

export function registerSuccessful(user){
    return{
        type: REGISTER_SUCCESSFUL,
        user
    }
}

export function registerFailed(){
    return{
        type: REGISTER_FAILED
    }
}


export function performRegister(user){
    return dispatch => {
    }
}