import { countries } from './countries'

export const isAuthenticated = () => {
  const auth = localStorage.getItem("auth");
  return auth ? true : false;
};

export const setAuth = token => {
  localStorage.setItem("auth", JSON.stringify(token));
};

export const getAuth = () => {
  const auth = localStorage.getItem("auth");
  return auth ? JSON.parse(localStorage.getItem("auth")) : {};
};

export const getParameterByName = name => {
  return decodeURIComponent(
    window.location.search.replace(
      new RegExp(
        "^(?:.*[&\\?]" +
          encodeURIComponent(name).replace(/[.+*]/g, "\\$&") +
          "(?:\\=([^&]*))?)?.*$",
        "i"
      ),
      "$1"
    )
  );
};

export const getCountries = () => {
  const data =  Object.keys(countries)
  return data.map((country,index) => {return {value:index, label:country}} )
}

export const getCities = (country) => {
  const data =  countries[country]
  return data.map((city,index) => {return {value:index, label:city}} )
}

export const getLocationName = (countryIndex, cityIndex) => {
  if(!countryIndex || !cityIndex){
    return "TBD"
  }
  const country  = Object.keys(countries).filter((data,index) => index === countryIndex)
  const city  = countries[country].filter((data,index) => index === cityIndex)
  return `${country}, ${city}`
}

export const getAllCities=()=>{
  const something = 
  [
    "Belgium",
    "France",
    "Luxembourg",
    "Netherlands",
    "Brazil",
    "United Kingdom"]
  let cities = []
  for(let country in something){
    console.log(something[country],countries[something[country]])
    let citiesArray = countries[something[country]].map(c => ({label: c, value: c}))
    cities = [...cities, ...citiesArray]
  }
  return cities
}