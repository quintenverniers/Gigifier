import React, { Component } from "react";

class SideProfile extends Component {
  render() {
    return (
      <div className="profile-sidebar">
        <span className="close-profile">
          <i className="la la-close" />
        </span>
        <div className="can-detail-s">
          <div className="cst">
            <img src="https://placehold.it/145x145" alt="" />
          </div>
          <h3> Chvrches</h3>
          <span>
            <i>Band</i>
          </span>
          <p>ArtistsNetworks088@gmail.com</p>
          <p>Member Since, 2017</p>
          <p>
            <i className="la la-map-marker" />Glassglow / Ireland
          </p>
        </div>
        {/* <div className="tree_widget-sec">
          <ul>
            <li className="inner-child active">
              <a  title="">
                <i className="la la-file-text" />My Profile
              </a>
              <ul style={{ display: "block" }}>
                <li>
                  <a  title="">
                    My Profile
                  </a>
                </li>
                <li>
                  <a  title="">
                    Social Network
                  </a>
                </li>
                <li>
                  <a  title="">
                    Contact Information
                  </a>
                </li>
              </ul>
            </li>
            <li className="inner-child">
              <a  title="">
                <i className="la la-user" />Job Alerts
              </a>
              <ul>
                <li>
                  <a  title="">
                    My Profile
                  </a>
                </li>
                <li>
                  <a  title="">
                    Social Network
                  </a>
                </li>
                <li>
                  <a  title="">
                    Contact Information
                  </a>
                </li>
              </ul>
            </li>
            <li className="inner-child">
              <a  title="">
                <i className="la la-flash" />Change Password
              </a>
              <ul>
                <li>
                  <a  title="">
                    My Profile
                  </a>
                </li>
                <li>
                  <a  title="">
                    Social Network
                  </a>
                </li>
                <li>
                  <a  title="">
                    Contact Information
                  </a>
                </li>
              </ul>
            </li>
            <li>
              <a  title="">
                <i className="la la-unlink" />Logout
              </a>
            </li>
          </ul>
        </div> */}
      </div>
    );
  }
}

export default SideProfile
