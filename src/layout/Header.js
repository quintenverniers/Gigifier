import React, { PureComponent } from "react"
import { Link } from "react-router-dom"
import NavLinks from "../components/Header/NavLinks"
import { connect } from "react-redux"
import ProfileMenu from "../components/Header/ProfileMenu"
import Notification from "../components/Header/Notification"

class Header extends PureComponent {
  state = {
    profileClicked: false
  }

  render() {
    const { type = "stick-top forsticky", isLoggedIn, isArtist } = this.props
    return (
      <header className={type}>
        <div className="menu-sec">
          <div className="container">
            <div className="logo">
              <Link to="/" title="Home">
                <img className="hidesticky" src="/images/logo.png" alt="" />
                <img className="showsticky" src="/images/logo.png" alt="" />
              </Link>
            </div>
            {isLoggedIn ? (
              <React.Fragment>
                <ProfileMenu />
                <Notification />
              </React.Fragment>
            ) : (
              <div className="btn-extars">
                <ul className="register-btns">
                  <li className="signup-popup">
                    <Link to="/register" title="Sign Up">
                      <i className="la la-key" /> Sign Up
                    </Link>
                  </li>
                  <li className="signin-popup">
                    <Link to="/login" title="Login">
                      <i className="la la-external-link-square" /> Login
                    </Link>
                  </li>
                  
                </ul>
              </div>
            )}
            <nav>
              <NavLinks showSearch={isLoggedIn} isArtist={isArtist} isLoggedIn={isLoggedIn}/>
            </nav>
          </div>
        </div>
      </header>
    )
  }
}

function mapStateToProps({ app }) {
  return {
    isLoggedIn: app.isLoggedIn,
    isArtist: app.isArtist
  }
}

export default connect(mapStateToProps)(Header)