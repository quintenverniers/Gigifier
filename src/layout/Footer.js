import React from "react"

export default function Footer({showScroll = true}) {
  return (
    <footer>
      <div className="bottom-line">
        <span>© 2018 Arists Network All rights reserved. </span>
        {showScroll &&   <a href="#scrollup" className="scrollup" title="">
          <i className="la la-arrow-up" />
        </a>}
       
      </div>
    </footer>
  )
}
