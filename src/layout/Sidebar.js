import React from "react"
import { Link } from "react-router-dom"
import { connect } from "react-redux"

const Sidebar = ({ isArtist }) => {
  return (
    <aside className="col-lg-3 column border-right">
      <div className="widget">
        <div className="tree_widget-sec">
          <ul>
            <li className="inner-child">
              {isArtist && (
                <Link to="/editprofile">
                  <i className="la la-file-text" />
                  Profile
                </Link>
              )}
              {!isArtist && (
                <Link to="/manage">
                  <i className="la la-file-text" />
                  Manage Jobs
                </Link>
              )}
               {!isArtist && (
                <Link to="/job">
                  <i className="la la-file-text" />
                  Post a New Job
                </Link>
              )}
            </li>
            {/* <li className="inner-child">
              <Link to="/editprofile">
                <i className="la la-flash" />
                Job Alerts
              </Link>
            </li> */}
            <li className="inner-child">
              <Link to="/password">
                <i className="la la-lock" />
                Change Password
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </aside>
  )
}

function mapStateToProps({ app }) {
  return {
    isArtist: app.isArtist
  }
}

export default connect(mapStateToProps)(Sidebar)
