import React, { PureComponent } from "react"
import { Link } from "react-router-dom"
import NavLinks from "../components/Header/NavLinks"
import { connect } from "react-redux"

class ResponsiveHeader extends PureComponent {
  componentDidMount() {
    window.$(".res-openmenu").on("click", function() {
      window.$(".responsive-header").addClass("active")
      window.$(".responsive-opensec").slideDown()
      window.$(".res-closemenu").removeClass("active")
      window.$(this).addClass("active")
    })
    window.$(".res-closemenu").on("click", function() {
      window.$(".responsive-header").removeClass("active")
      window.$(".responsive-opensec").slideUp()
      window.$(".res-openmenu").removeClass("active")
      window.$(this).addClass("active")
    })
  }

  render() {
    return (
      <div className="responsive-header">
        <div className="responsive-menubar">
          <div className="res-logo">
            <Link to="/" title="">
              <img src="/images/logo.png" alt="" />
            </Link>
          </div>
          <div className="menu-resaction">
            <div className="res-openmenu">
              <img src="/images/icon.png" alt="" /> Menu
            </div>
            <div className="res-closemenu">
              <img src="/images/icon2.png" alt="" /> Close
            </div>
          </div>
        </div>
        <div className="responsive-opensec">
        
          {!this.props.isLoggedIn ? (
            <div className="btn-extars">
              <ul className="account-btns">
                <li>
                  <Link to="/register" title="">
                    <i className="la la-key" /> Sign Up
                  </Link>
                </li>
                <li>
                  <Link to="/login" title="">
                    <i className="la la-external-link-square" /> Login
                  </Link>
                </li>
              </ul>
            </div>
          ) : (
            <div className="btn-extars">
              <ul className="account-btns">
                {!this.props.isArtist && (
                  <li>
                    <Link to="/manage">
                      <i className="la la-briefcase" /> Manage Postings
                    </Link>
                  </li>
                )}
                {!this.props.isArtist && (
                  <li>
                    <Link to="/job">
                      <i className="la la-briefcase" /> Post a New Job
                    </Link>
                  </li>
                )}
                {this.props.isArtist && (
                  <li>
                    <Link to="/editprofile">
                      <i className="la la-leaf" /> Edit Profile
                    </Link>
                  </li>
                )}
                <li>
                  <Link to="/password">
                    <i className="la la-key" /> Change Password
                  </Link>
                </li>
                <li>
                  <Link to="/userlogout">
                    <i className="la la-history" /> Logout
                  </Link>
                </li>
              </ul>
            </div>
          )}

          <div className="responsivemenu">
          <NavLinks showSearch={this.props.isLoggedIn} isArtist={this.props.isArtist} isLoggedIn={this.props.isLoggedIn}/>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps({ app }) {
  return {
    isLoggedIn: app.isLoggedIn,
    isArtist: app.isArtist
  }
}

export default connect(mapStateToProps)(ResponsiveHeader)
