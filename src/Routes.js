import React, { Component } from "react"
import { Route, Switch } from "react-router-dom"

import AuthorizedRoute from "./components/AuthorizedRoute"
import Profile from "./pages/Profile"
import Login from "./pages/Login"
import Logout from "./pages/Logout"
import Register from "./pages/Register"
import Main from "./pages/Main"
import NotFound from "./pages/NotFound"
import Callback from "./pages/Callback"
import ChangePassword from "./pages/ChangePassword"
import SearchEvents from "./pages/SearchEvents"
import EditProfile from "./pages/EditProfile"
import SearchArtists from "./pages/SearchArtists"
import { ToastContainer } from "react-toastify"
import ResetPassword from "./pages/ResetPassword";
import Terms from "./pages/Terms";
import Privacy from "./pages/Privacy";
import ForgotPassword from "./pages/ForgotPassword";
import ManageJobs from "./pages/ManageJobs";
import PostJob from "./pages/PostJob";
import Event from "./pages/Event";
import ContactUs from "./pages/ContactUs";

class Routes extends Component {
  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route path="/" exact component={Main} />
          <Route  path="/profile/:id" component={Profile} />
          <Route  path="/event/:id" component={Event} />
          <AuthorizedRoute path="/password" component={ChangePassword} />
          <AuthorizedRoute path="/editprofile" component={EditProfile} />
          <Route path="/resetpassword" component={ResetPassword} />
          <Route path="/forgotpassword" component={ForgotPassword} />
          <Route path="/login" component={Login} />
          <Route path="/userlogout" component={Logout} />
          <Route path="/register" component={Register} />
          <Route path="/search/events" component={SearchEvents} />
          <Route path="/search/artists" component={SearchArtists} />
          <Route path="/auth/account" component={Callback} />
          <AuthorizedRoute path="/manage" component={ManageJobs} />
          <AuthorizedRoute path="/job" component={PostJob} />
          <Route path="/auth/account" component={Callback} />
          <Route path="/terms" component={Terms} />
          <Route path="/privacy" component={Privacy} />
          <Route path="/contactus" component={ContactUs} />
          <Route path="*" component={NotFound} />
        </Switch> 
        <ToastContainer
          position="bottom-right"
          autoClose={5000}
          hideProgressBar
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnVisibilityChange
          draggable
          pauseOnHover
        />
      </React.Fragment>
    )
  }
}

export default Routes
