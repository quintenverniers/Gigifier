const load = () => {
  window.$(".tab-sec li a").on("click", function() {
    var tab_id = window.$(this).attr("data-tab")
    window.$(".tab-sec li a").removeClass("current")
    window.$(".tab-content").removeClass("current")
    window.$(this).addClass("current")
    window.$("#" + tab_id).addClass("current")
  })

}

export default load