const load = () => {
  window.$(".main-slider-sec").not('.slick-initialized').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    autoplay: false,
    slide: "li",
    fade: false,
    infinite: true,
    dots: false
  })
}
export default load
