const widgettree = () => {
    window.$("#toggle-widget .content").hide()
    window
      .$("#toggle-widget h2:first")
      .addClass("active")
      .next()
      .slideDown("fast")
    window.$("#toggle-widget h2").on("click", function() {
      if (
        window
          .$(this)
          .next()
          .is(":hidden")
      ) {
        window
          .$("#toggle-widget h2")
          .removeClass("active")
          .next()
          .slideUp("fast")
        window
          .$(this)
          .toggleClass("active")
          .next()
          .slideDown("fast")
      }
    })

    window.$(".tree_widget-sec > ul > li.inner-child:first > ul").slideDown()
    window.$(".tree_widget-sec > ul > li.inner-child:first").addClass("active")
    window
      .$(".tree_widget-sec > ul > li.inner-child > a")
      .on("click", function() {
        window.$(".tree_widget-sec > ul > li.inner-child").removeClass("active")
        window.$(".tree_widget-sec > ul > li > ul").slideUp()
        window
          .$(this)
          .parent()
          .addClass("active")
        window
          .$(this)
          .next("ul")
          .slideDown()
        return false
      })


}
  
  export default widgettree
  