const slick = () => {
  window.$("#companies-carousel").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    autoplay: true,
    slide: "div",
    fade: false,
    infinite: true,
    dots: false,
    responsive: [
      {
        breakpoint: 980,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          vertical: false,
          centerMode: false,
          centerPadding: "0px",
          dots: true,
          arrows: false
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          vertical: false,
          centerMode: false,
          centerPadding: "0px",
          dots: true,
          arrows: false
        }
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          vertical: false,
          centerMode: false,
          centerPadding: "0px",
          dots: true,
          arrows: false
        }
      }
    ]
  })

  /*================== BG Slide Animation =====================*/
  window.$("#team-carousel").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: false,
    autoplay: false,
    slide: "div",
    fade: false,
    infinite: true,
    dots: true,
    responsive: [
      {
        breakpoint: 980,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          vertical: false,
          centerMode: false,
          centerPadding: "0px",
          dots: true,
          arrows: false
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          vertical: false,
          centerMode: false,
          centerPadding: "0px",
          dots: true,
          arrows: false
        }
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          vertical: false,
          centerMode: false,
          centerPadding: "0px",
          dots: true,
          arrows: false
        }
      }
    ]
  })

  /*================== BG Slide Animation =====================*/
  window.$("#reviews").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: false,
    slide: "div",
    fade: false,
    infinite: true,
    dots: true
  })

  /*================== BG Slide Animation =====================*/
  window.$("#reviews-carousel").slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    slide: "div",
    fade: false,
    infinite: true,
    dots: true,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          vertical: false,
          centerMode: true,
          centerPadding: "0px",
          dots: true
        }
      }
    ]
  })

  /*================== BG Slide Animation =====================*/
  window.$(".main-slider-sec").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    autoplay: false,
    slide: "li",
    fade: false,
    infinite: true,
    dots: false
  })

  /*================== Sports =====================*/
  window.$(".sport-wide-posts").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    slide: "li",
    fade: false,
    asNavFor: ".sport-wide-navs"
  })
  window.$(".sport-wide-navs").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: ".sport-wide-posts",
    dots: false,
    arrows: false,
    slide: "li",
    vertical: true,
    centerMode: true,
    centerPadding: "0px",
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          vertical: true,
          centerMode: true,
          centerPadding: "0px",
          dots: false
        }
      },
      {
        breakpoint: 980,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          vertical: false,
          centerMode: true,
          centerPadding: "0px",
          dots: false
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          vertical: false,
          centerMode: true,
          centerPadding: "0px",
          dots: false
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          vertical: false,
          centerMode: true,
          centerPadding: "0px",
          dots: false
        }
      }
    ]
  })
}

export default slick
