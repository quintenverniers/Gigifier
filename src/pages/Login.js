import React, { Component } from "react"
import { connect } from "react-redux"
import { loginSuccessful } from "../actions"
import { push } from "react-router-redux"
import onLoad from "../jqueryscripts/onLoad"
import CSSTransitionGroup from "react-addons-css-transition-group"
import Footer from "../layout/Footer"
import Header from "../layout/Header"
import ResponsiveHeader from "../layout/ResponsiveHeader"
import { Link } from "react-router-dom"
import { toast } from "react-toastify"
import qs from "qs"
import Loader from "../components/Loader"
import FacebookLogin from "react-facebook-login"
class Login extends Component {
  state = {
    username: "",
    password: "",
    confirmed: false,
    search: false,
    loading: false,
    error: false,
    message: ""
  }

  componentDidMount() {
    /*onLoad()
    if (this.props.isLoggedIn) {
      if (this.props.isArtist == null) {
        getCurrentUser(this.props.userid).then(response => {
          if (response.userType === "artist") {
            if (!response.description) {
              this.props.push(`/editprofile?mandatory=true`)
            }
            this.props.push(`/profile/${this.props.userid}`)
          } else {
            this.props.push(`/manage/`)
          }
        })
      } else {
        if (this.props.isArtist) {
          this.props.push(`/profile/${this.props.userid}`)
        } else {
          this.props.push(`/manage/`)
        }
      }
    }
    const { confirmed, search } = qs.parse(this.props.location.search, {
      ignoreQueryPrefix: true
    })
    if (confirmed) {
      this.setState({ confirmed })
    }

    if (search) {
      this.setState({ search })
    }*/
    //this.props.push('/profile/2')
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  submit = () => {
    const { username, password } = this.state
    console.log(username, password)
  }

  render() {
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div className="theme-layout" id="scrollup">
          <ResponsiveHeader />
          <Header type="gradient" />

          <section>
            <div className="block no-padding  gray">
              <div className="container">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="inner2">
                      <div className="inner-title2">
                        <h3>Login</h3>
                        <span>Keep up to date with the latest news</span>
                      </div>
                      <div className="page-breacrumbs">
                        <ul className="breadcrumbs">
                          <li>
                            <Link to="/" href="#" title="">
                              Home
                            </Link>
                          </li>
                          <li>
                            <Link to="/" href="#" title="">
                              Pages
                            </Link>
                          </li>
                          <li>
                            <Link to="/login" href="#" title="">
                              Login
                            </Link>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="block remove-bottom">
              <div className="container">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="account-popup-area signin-popup-box static">
                      <div className="account-popup">
                        {this.state.confirmed && (
                          <span>
                            Email address is confirmed. You may now login.
                          </span>
                        )}
                        {this.state.search && (
                          <span>Login to be able to search.</span>
                        )}
                        <form>
                          <div className="cfield">
                            <input
                              type="text"
                              name="username"
                              placeholder="Username"
                              value={this.state.username}
                              onChange={this.handleChange}
                            />
                            <i className="la la-user" />
                          </div>
                          <div className="cfield">
                            <input
                              type="password"
                              name="password"
                              placeholder="Password"
                              value={this.state.password}
                              onChange={this.handleChange}
                            />
                            <i className="la la-key" />
                          </div>
                          
                          {this.state.error && <div>{this.state.message}</div>}
                          <Link to="/forgotpassword" title="Forgot Password">
                            Forgot Password?
                          </Link>
                          <button type="button" onClick={this.submit}>
                            Login
                          </button>
                        </form>
                        <div className="extra-login">
                          <span>Or</span>
                          <div className="login-social">
                            
                            <FacebookLogin
                              appId="1373362946099639"
                              fields="name,email"
                              cssClass="fa fa-facebook"
                              callback={this.responseFacebook}
                              textButton=""
                              tag="a"
                              typeButton=""
                              redirectUri="https://gigifier.com/login"
                            />
                       
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <Footer />
        </div>
        <Loader loading={this.state.loading} />
      </CSSTransitionGroup>
    )
  }
}

function mapStateToProps({ app }) {
  return {
    isLoggedIn: app.isLoggedIn,
    userid: app.userid,
    isArtist: app.isArtist
  }
}

export default connect(
  mapStateToProps,
  { push, loginSuccessful }
)(Login)
