import React, { Component } from "react"
import { connect } from "react-redux"
import CSSTransitionGroup from "react-addons-css-transition-group"
import { push } from "react-router-redux"
import Slider from "react-slick"
import load from "../jqueryscripts/profile"
import onLoad from "../jqueryscripts/onLoad"
import ResponsiveHeader from "../layout/ResponsiveHeader"
import Footer from "../layout/Footer"
import Header from "../layout/Header"
import {
  getCurrentUser,
  getUserVideos,
  getUserGallery,
  getUserSoundCloud,
  getUpcomingGigs,
  getArtistReviews,
  saveReview
} from "../services"
import { getCountries, getCities } from "../helper"
import Carousel from "react-image-carousel"
import GigItem from "../components/GigItem"
import { Link } from "react-router-dom"
import moment from "moment"
import { toast } from "react-toastify"
import Loader from "../components/Loader"

const ArtistTypes = [
  { value: "solo", label: "Solo Artist" },
  {
    value: "band",
    label: "Band"
  },
  {
    value: "instrument",
    label: "Instrument Player"
  },
  { value: "orchestra", label: "Orchestra" }
]

const CountryOption = getCountries()

class Profile extends Component {
  state = {
    email: "",
    location: "",
    name: "",
    artistType: "",
    tags: [],
    description: "",
    gigs: [],
    reviews: [],
    video: "",
    soundcloud: "",
    photo: "",
    website: "",
    facebook: "",
    linkedin: "",
    twitter: "",
    instagram: "",
    telephoneNumber: "",
    artistVideos: {
      videoId: "",
      videoTitle: "",
      videoUrl: ""
    },
    artistGallery: [],
    artistSoundCloud: {
      soundCloudId: "",
      soundCloudTitle: "",
      soundCloudUrl: ""
    },
    comment: "",
    loading: false
  }
  componentDidMount() {

  }
  
  render() {
    const image = this.state.profPicUrl
      ? `https://gigifier.com/server${this.state.profPicUrl}`
      : "/images/portrait.png"
    const embedVideo = this.state.artistVideos.videoUrl.split("=")[1]
    var images = this.state.artistGallery
    const galleryList = []
    Object.keys(images).forEach(function(keyName, keyIndex) {
      galleryList.push("https://gigifier.com/server" + images[keyName])
    })

    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div className="theme-layout" id="scrollup">
          <ResponsiveHeader />
          <Header />
          <section className="overlape">
            <div className="block no-padding">
              <div
                data-velocity="-.1"
                style={{
                  background:
                    "url(/images/bg_header_artist.jpg) repeat scroll 50% 422.28px transparent"
                }}
                className="parallax scrolly-invisible no-parallax"
              />
              <div className="container fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="inner-header">
                      <div className="container">
                        <div className="row">
                          <div className="col-lg-6">
                            <div className="text-socail">
                             
                            </div>
                          </div>
                        
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section className="overlape">
            <div className="block remove-top">
              <div className="container">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="cand-single-user">
                      <div className="share-bar circle" />
                      <div className="can-detail-s">
                        <div className="cst">
                          <img
                            src={image}
                            height="150px"
                            width="150px"
                            alt=""
                          />
                        </div>
                        <h3>{this.state.name || "Artist"}</h3>
                        <span>{this.state.artistType || "Artist"}</span>
                        <div className="skills-badge">
                          {this.state.tags &&
                            this.state.tags.map(tag => <span>{tag}</span>)}
                        </div>
                      </div>
                      <div className="download-cv" />
                    </div>
                    <div className="cand-details-sec">
                      <div className="row no-gape">
                        <div className="col-lg-9 column">
                          <div className="cand-details">
                            <div className="job-overview style2">
                              <div className="tab-sec">
                                {this.state.artistVideos.videoUrl && (
                                  <div id="videos" className="tab-content">
                                    <iframe
                                      width="420"
                                      height="500"
                                      title="video"
                                      src={`https://www.youtube.com/embed/${embedVideo}`}
                                      frameBorder="0"
                                      allowFullScreen
                                    />
                                  </div>
                                )}
                                <div
                                  id="photos"
                                  className="tab-content current"
                                >
                                  <div className="job-listings-tabs">
                                    {galleryList.length !== 0 ? (
                                      <div
                                        style={{
                                          height: 500,
                                          maxWidth: "100%",
                                          maxGeight: "100vh",
                                          margin: "0 auto"
                                        }}
                                      >
                                        <Carousel
                                          images={galleryList}
                                          thumb={false}
                                          loop={true}
                                          autoplay={5000}
                                        />
                                      </div>
                                    ) : (
                                      <p>No Gallery Available</p>
                                    )}
                                  </div>
                                </div>
                                {this.state.artistSoundCloud.soundCloudUrl && (
                                  <div id="soundcloud" className="tab-content">
                                    <div className="job-listings-tabs">
                                      {this.state.artistSoundCloud
                                        .soundCloudUrl ? (
                                        this.state.artistSoundCloud.soundCloudUrl.includes(
                                          "iframe"
                                        ) ? (
                                          <div
                                            dangerouslySetInnerHTML={{
                                              __html: this.state
                                                .artistSoundCloud.soundCloudUrl
                                            }}
                                          />
                                        ) : (
                                          <div>
                                            Invalid Soundcloud Embeded Link
                                          </div>
                                        )
                                      ) : (
                                        <p>No Soundcloud available</p>
                                      )}
                                    </div>
                                  </div>
                                )}
                                <ul className="nav nav-tabs">
                                  {this.state.artistVideos.videoUrl && (
                                    <li>
                                      <a data-tab="videos">Videos</a>
                                    </li>
                                  )}
                                  <li>
                                    <a className="current" data-tab="photos">
                                      Photos
                                    </a>
                                  </li>
                                  {this.state.artistSoundCloud
                                    .soundCloudUrl && (
                                    <li>
                                      <a data-tab="soundcloud">Soundcloud</a>
                                    </li>
                                  )}
                                </ul>
                              </div>
                            </div>
                            <h2>Artist Overview</h2>
                            <p>{this.state.description || "New Artist"}</p>

                            <h2>Reviews</h2>
                            <div className="reviews-sec" id="reviews">
                              {this.state.reviews.length > 0 ? (
                                this.state.reviews.map(review => {
                                  console.log(review)
                                  return (
                                    <React.Fragment>
                                      <div className="row">
                                        <div className="col-sm-3">
                                          <img
                                            src={
                                              review.reviewerPic
                                                ? review.reviewerPic ===
                                                  "/images/portrait.png"
                                                  ? "/images/portrait.png"
                                                  : `https://gigifier.com/server${
                                                      review.reviewerPic
                                                    }`
                                                : "/images/portrait.png"
                                            }
                                            alt='gigifier profile pic'
                                            style={{ width: 60, height: 60 }}
                                            className="img-rounded"
                                          />
                                          <div className="review-block-name">
                                            <a href="#">
                                              {review.reviewerName}
                                            </a>
                                          </div>
                                          <div className="review-block-date">
                                            {moment(review.reviewDate).format(
                                              "MMMM	DD, YYYY"
                                            )}
                                            <br />
                                            {moment().to(review.reviewDate)}
                                          </div>
                                        </div>
                                        <div className="col-sm-9">
                                          <div className="review-block-description">
                                            {review.reviewDetails}
                                          </div>
                                        </div>
                                      </div>
                                      <hr />
                                    </React.Fragment>
                                  )
                                })
                              ) : (
                                <p>No Reviews available</p>
                              )}
                            </div>
                            {this.props.isLoggedIn &&
                              this.props.match.params.id !==
                                this.props.userid && (
                                <form>
                                  <textarea
                                    name="comment"
                                    placeholder=" Write your review.."
                                    onChange={this.handleChange}
                                    value={this.state.comment}
                                  />
                                  <br /> <br />
                                  <div className="pf-field">
                                    <button
                                      style={{ marginTop: 17 }}
                                      onClick={this.saveComment}
                                    >
                                      Submit
                                    </button>
                                  </div>
                                </form>
                              )}
                          </div>
                        </div>
                        <div className="col-lg-3 column">
                          {this.props.isLoggedIn ? (
                            <div className="job-overview style3">
                              {this.props.match.params.id !==
                                this.props.userid && (
                                <a
                                  href={`mailto:${this.state.email}`}
                                  class="apply-thisjob"
                                  title=""
                                >
                                  <i class="la la-paper-plane" />
                                  Hire / Collaborate
                                </a>
                              )}
                              <h2 className="upcominggigs">Contact Details</h2>
                              <ul>
                                <li>
                                  <i className="la la-envelope" />
                                  <h3>{this.state.email}</h3>
                                </li>
                                <li>
                                  <i className="la la-phone" />
                                  <h3>{this.state.telephoneNumber || "TBD"}</h3>
                                </li>
                                <li>
                                  <i className="la la-map-marker" />
                                  <h3>{this.state.location || "TBD"}</h3>
                                </li>
                              </ul>
                             
                            </div>
                          ) : (
                            <div
                              className="simple-text-block"
                              style={{ margin: "0 20px" }}
                            >
                              <h2
                                className="upcominggigs"
                                style={{ textAlign: "left" }}
                              >
                                Contact Details
                              </h2>
                              <Link
                                to="/register"
                                title="Register"
                                style={{ marginTop: 0 }}
                              >
                                Sign up to view details
                              </Link>
                            </div>
                          )}
                          <div className="job-overview style3">
                            <h2 className="upcominggigs"> Social Links</h2>
                            <div className="edu-history-sec">
                             
                              {this.state.facebook && (
                                <a
                                  target="_blank"
                                  title={this.state.facebook}
                                  href={this.state.facebook}
                                >
                                  <i className="falinks fa-facebook" />
                                </a>
                              )}
                              {this.state.twitter && (
                                <a
                                  target="_blank"
                                  title={this.state.twitter}
                                  href={this.state.twitter}
                                >
                                  <i className="falinks fa-twitter" />
                                </a>
                              )}
                              {this.state.instagram && (
                                <a
                                  target="_blank"
                                  title={this.state.instagram}
                                  href={this.state.instagram}
                                >
                                  <i className="falinks fa-instagram" />
                                </a>
                              )}
                              {this.state.linkedin && (
                                <a
                                  target="_blank"
                                  title={this.state.linkedin}
                                  href={this.state.linkedin}
                                >
                                  <i className="falinks fa-linkedin" />
                                </a>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <Footer />
        </div>
        <Loader loading={this.state.loading} />
      </CSSTransitionGroup>
    )
  }
}

function mapStateToProps({ app }) {
  return {
    isLoggedIn: app.isLoggedIn,
    userid: app.userid,
    name: app.name,
    email: app.email,
    profilepic: app.profilepic
  }
}

export default connect(
  mapStateToProps,
  { push }
)(Profile)
