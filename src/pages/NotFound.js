import React, { Component } from 'react'
import { Link } from "react-router-dom";


class NotFound extends Component {
    render() {
        return (
            <div className="theme-layout" id="scrollup">
                <section>
                    <div className="block no-padding">
                        <div className="container fluid">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="main-featured-sec witherror">
                                        <ul className="main-slider-sec text-arrows">
                                            <li><img src="/images/404_1.jpg" alt="" /></li>
                                        </ul>
                                        <div className="error-sec">
                                            <img src="/images/404.png" alt="" />
                                            <span>We Are Sorry, Page Not Found</span>
                                            <p>Unfortunately the page you were looking for could not be found. It may be temporarily unavailable, moved or no longer exist. Check the Url you entered for any mistakes and try again.</p>
                                            <form>
                                                <input type="text" placeholder="Enter any Keyword" /><button type="submit"><i className="la la-search"></i></button>
                                            </form>
                                            <h6><Link to="/" title="Home">Back To Homepage</Link></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default NotFound