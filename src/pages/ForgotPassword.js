import React, { Component } from "react"
import CSSTransitionGroup from "react-addons-css-transition-group"
import Footer from "../layout/Footer"
import ResponsiveHeader from "../layout/ResponsiveHeader"
import Header from "../layout/Header"
import SideProfile from "../layout/SideProfile"
import { toast } from "react-toastify";

class ForgotPassword extends Component {
  state = {
    email: "",
    succesful: null
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  submit = () => {
  }


  render() {
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div className="theme-layout" id="scrollup">
          <ResponsiveHeader />
          <Header type="stick-top" />
          <section className="overlape">
            <div className="block no-padding">
              <div
                data-velocity="-.1"
                style={{
                  background:
                    "url(images/bg_header_artist.jpg) repeat scroll 50% 422.28px transparent"
                }}
                className="parallax scrolly-invisible no-parallax"
              />
              <div className="container fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="inner-header">
                      <h3>Welcome</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="block no-padding">
              <div className="container">
                <div className="row no-gape">
                {this.state.successfull ? (
                    <div className="col-lg-12">
                      <p>Password Reset link was sent to your email</p>
                    </div>
                  ) : (
                  <div className="col-lg-9 column">
                    <div className="padding-left">
                      <div className="manage-jobs-sec">
                        <h3>Forgot Password</h3>
                        <div className="change-password">
                          <form>
                            <div className="row">
                              <div className="col-lg-6">
                                <span className="pf-title">Email Address</span>
                                <div className="pf-field">
                                  <input
                                    type="text"
                                    name="email"
                                    onChange={this.handleChange}
                                    value={this.state.email}
                                  />
                                </div>
                                <button type="button" onClick={this.submit}>Request Reset Password</button>
                              </div>
                              <div className="col-lg-6">
                                <i className="la la-key big-icon" />
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  )}
                </div>
              </div>
            </div>
          </section>

          <Footer />
        </div>
        <SideProfile />
      </CSSTransitionGroup>
    )
  }
}

export default ForgotPassword
