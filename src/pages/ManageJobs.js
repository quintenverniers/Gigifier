import React, { Component } from "react"
import { connect } from "react-redux"
import { push } from "react-router-redux"
import CSSTransitionGroup from "react-addons-css-transition-group"
import Footer from "../layout/Footer"
import Header from "../layout/Header"
import SideProfile from "../layout/SideProfile"
import Sidebar from "../layout/Sidebar"
import ResponsiveHeader from "../layout/ResponsiveHeader"
import JobItem from "../components/JobItem"
import { setImage } from "../actions"
import Loader from "../components/Loader"
import load from "../jqueryscripts/profile"
import onLoad from "../jqueryscripts/onLoad"
class ManageJobs extends Component {
  state = {
    jobs: []
  }
  componentDidMount(){
    
  }
  componentDidUpdate() {
    load()
    onLoad()
  }
  componentWillReceiveProps(nextProps) {
    
  }

  render() {
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div className="theme-layout" id="scrollup">
          <ResponsiveHeader />
          <Header />

          <section className="overlape">
            <div className="block no-padding">
              <div
                data-velocity="-.1"
                style={{
                  background:
                    "url(/images/bg_header_artist.jpg) repeat scroll 50% 422.28px transparent"
                }}
                className="parallax scrolly-invisible no-parallax"
              />
              <div className="container fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="inner-header">
                      <h3>Manage Job Posted</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="block no-padding">
              <div className="container">
                <div className="row no-gape">
                  <Sidebar />
                  <div className="col-lg-9 column">
                    <div className="padding-left">
                      <div className="manage-jobs-sec">
                        <h3>Manage Jobs</h3>
                        <div className="extra-job-info">
                          <span>
                            <i className="la la-clock-o" />
                            <strong>{this.state.jobs.length}</strong> Job Posted
                          </span>
                       
                        </div>
                        <table>
                          <thead>
                            <tr>
                              <td>Title</td>
                              <td>Created</td>
                              <td>Action</td>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.jobs.length > 0 &&
                              this.state.jobs.map(job => {
                                return (
                                  <JobItem
                                    key={job._id}
                                    eventName={job.eventName}
                                    eventCity={job.eventCity}
                                    eventCountry={job.eventCountry}
                                    eventCreated={job.eventCreated}
                                    id={job._id}
                                  />
                                )
                              })}
                         
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <Footer />
        </div>
        <SideProfile />
        <Loader loading={false} />
      </CSSTransitionGroup>
    )
  }
}

function mapStateToProps({ app }) {
  return {
    isLoggedIn: app.isLoggedIn,
    userid: app.userid,
    isArtist: app.isArtist
  }
}

export default connect(
  mapStateToProps,
  { push, setImage }
)(ManageJobs)
