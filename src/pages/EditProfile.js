import React, { Component } from "react"
import { connect } from "react-redux"
import { push } from "react-router-redux"
import CSSTransitionGroup from "react-addons-css-transition-group"
import Footer from "../layout/Footer"
import Header from "../layout/Header"
import Select from "react-select"
import SideProfile from "../layout/SideProfile"
import Sidebar from "../layout/Sidebar"
import ResponsiveHeader from "../layout/ResponsiveHeader"
import { getCountries, getCities } from "../helper"
import { toast } from "react-toastify"
import { setImage, setNameOnly, setRedirect } from "../actions"
import ImageUploader from "react-images-upload"
import Loader from "../components/Loader"
import qs from "qs"

const ExperienceOption = [
  { value: 1, label: "0-2 Years" },
  { value: 2, label: "2-6 Years" },
  {
    value: 3,
    label: "6-12 Years"
  },
  {
    value: 4,
    label: "More than 12 Years"
  }
]

const AgeOption = [
  { value: 1, label: "12-22 Years" },
  { value: 2, label: "22-30 Years" },
  {
    value: 3,
    label: "30-40 Years"
  },
  {
    value: 4,
    label: "40-50 Years"
  },
  {
    value: 5,
    label: "More than 50 Years"
  }
]

const CategoryTags = [
  { value: 1, label: "Guitar" },
  { value: 2, label: "Drum" },
  { value: 3, label: "Concert" },
  { value: 4, label: "Acapela" },
  { value: 5, label: "Freestyle" },
  { value: 6, label: "Musician" },
  { value: 7, label: "Comedian" },
  { value: 8, label: "Actor" },
  { value: 9, label: "Theater" },
  { value: 10, label: "Painter" },
  { value: 11, label: "Writer" },
  { value: 12, label: "Other" }
]

const ArtistTypes = [
  { value: "solo", label: "Solo Artist" },
  {
    value: "band",
    label: "Band"
  },
  {
    value: "instrument",
    label: "Instrument Player"
  },
  { value: "orchestra", label: "Orchestra" }
]

const CountryOption = getCountries()

class EditProfile extends Component {
  componentDidMount() {
    const { userid } = this.props
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.country !== this.state.country) {
      this.setState({ cityOptions: getCities(this.state.country.label) })
    }
  }

  fileInput = React.createRef()
  imageInput = React.createRef()

  state = {
    name: "",
    artistType: "",
    // allowsearch: null,
    experience: null,
    age: null,
    currentrate: "",
    artistid: "",
    id: "",
    description: "",
    country: "",
    city: "",
    telephoneNumber: "",
    categories: null,
    email: "",
    website: "",
    facebook: "",
    linkedin: "",
    twitter: "",
    instagram: "",
    cityOptions: [],
    profPicUrl: "",
    successful: "",
    artistVideos: {
      videoId: "",
      videoTitle: "",
      videoUrl: ""
    },
    artistGallery: [],
    artistSoundCloud: {
      soundCloudId: "",
      soundCloudTitle: "",
      soundCloudUrl: ""
    },
    loading: false
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  handleArtistGalChange = (event, name) => {
    this.setState({
      artistVideos: {
        ...this.state.artistVideos,
        [name]: event.target.value
      }
    })
  }

  handleArtistSCChange = (event, name) => {
    this.setState({
      artistSoundCloud: {
        ...this.state.artistSoundCloud,
        [name]: event.target.value
      }
    })
  }

  chosenHandleChange = (event, name) => {
    this.setState({
      [name]: event
    })
  }

  onDrop = picture => {
    this.setState({
      artistGallery: picture
    })
  }

  updateProfile = () => {
    const data = {
      ...(this.state.name && { name: this.state.name }),
    
      ...(this.state.experience && { experience: this.state.experience.value }),
      ...(this.state.age && { age: this.state.age.value }),
      ...(this.state.artistType && { artistType: this.state.artistType.value }),
      ...(this.state.currentrate && { currentrate: this.state.currentrate }),
      ...(this.state.artistid && { artistid: this.state.artistid }),
      ...(this.state.id && { id: this.state.id }),
      ...(this.state.description && { description: this.state.description }),
      ...(this.state.country && { country: this.state.country.value }),
      ...(this.state.city && { city: this.state.city.value }),
      ...(this.state.telephoneNumber && {
        telephoneNumber: this.state.telephoneNumber
      }),
      ...(this.state.categories && {
        categories: this.state.categories.map(category => category.value)
      }),
      ...(this.state.email && { email: this.state.email }),
      ...(this.state.website && { website: this.state.website }),
      ...(this.state.facebook && { facebook: this.state.facebook }),
      ...(this.state.linkedin && { linkedin: this.state.linkedin }),
      ...(this.state.twitter && { twitter: this.state.twitter }),
      ...(this.state.instagram && { instagram: this.state.instagram })
    }

    if (
      !this.state.name ||
      !this.state.experience ||
      !this.state.age ||
      !this.state.artistType ||
      !this.state.categories ||
      !this.state.description ||
      !this.state.country ||
      !this.state.city
    ) {
      toast.error("Please fill up required fields.")
      return
    }
    if (this.state.categories.length < 3) {
      toast.error("Minimum of 3 cateogries")
      return
    }

    if (this.state.description.length < 300) {
      toast.error("Description should have at least 300 characters")
      return
    }
    this.setState({ loading: true })

  }

  updatePicture = () => {
    
  }

  updateVideos = () => {
    
  }

  updateGallery = () => {
    
  }

  updateSoundCloud = () => {
  
  }

  render() {
    const { mandatory } = qs.parse(this.props.location.search, {
      ignoreQueryPrefix: true
    })
    const image = this.state.profPicUrl
      ? `https://gigifier.com/server${this.state.profPicUrl}`
      : "/images/portrait.png"
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div className="theme-layout" id="scrollup">
          <ResponsiveHeader />
          <Header />

          <section className="overlape">
            <div className="block no-padding">
              <div
                data-velocity="-.1"
                style={{
                  background:
                    "url(images/bg_header_artist.jpg) repeat scroll 50% 422.28px transparent"
                }}
                className="parallax scrolly-invisible no-parallax"
              />
              <div className="container fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="inner-header">
                      <h3>Welcome</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="block no-padding">
              <div className="container">
                <div className="row no-gape">
                  <Sidebar />
                  <div className="col-lg-9 column">
                    <div className="padding-left">
                      <div className="profile-title contact-edit">
                        <h3>
                          My Profile{" "}
                          {mandatory && (
                            <span style={{ fontSize: 10, verticalAlign: 3 }}>
                              {" "}
                              - Profile update required
                            </span>
                          )}
                        </h3>
                        <form>
                          <div className="upload-img-bar">
                            <span className="round">
                              <img
                                src={image}
                                height="150px"
                                width="150px"
                                alt=""
                              />
                              {/* <i>x</i> */}
                            </span>
                            <div className="upload-info">
                              <input type="file" ref={this.fileInput} />
                              <span>
                                Max file size is 1MB, Minimum dimension: 270x210
                                And Suitable files are .jpg & .png
                              </span>
                            </div>
                          </div>
                          <div className="col-lg-12">
                            <button type="button" onClick={this.updatePicture}>
                              Update Picture
                            </button>
                          </div>
                        </form>
                      </div>
                      <div className="profile-form-edit">
                        <form>
                          <div className="row">
                            <div className="col-lg-6">
                              <span className="pf-title">Name</span>
                              <div className="pf-field">
                                <input
                                  type="text"
                                  placeholder="Name"
                                  name="name"
                                  value={this.state.name}
                                  onChange={this.handleChange}
                                />
                                <i className="required">*</i>
                              </div>
                            </div>
                            <div className="col-lg-6">
                              <span className="pf-title">Job Title</span>
                              <div className="pf-field">
                                <Select
                                  className="chosen-container"
                                  classNamePrefix="chosen"
                                  placeholder="Job Title"
                                  value={this.state.artistType}
                                  onChange={event =>
                                    this.chosenHandleChange(event, "artistType")
                                  }
                                  options={ArtistTypes}
                                />
                                <i className="required">*</i>
                              </div>
                            </div>
                         
                            <div className="col-lg-6">
                              <span className="pf-title">Experience</span>
                              <div className="pf-field">
                                <Select
                                  placeholder="Select Experience"
                                  value={this.state.experience || null}
                                  onChange={event =>
                                    this.chosenHandleChange(event, "experience")
                                  }
                                  options={ExperienceOption}
                                />
                              </div>
                            </div>
                            <div className="col-lg-6">
                              <span className="pf-title">Age</span>
                              <div className="pf-field">
                                <Select
                                  placeholder="Select Age"
                                  value={this.state.age}
                                  onChange={event =>
                                    this.chosenHandleChange(event, "age")
                                  }
                                  options={AgeOption}
                                />
                                <i className="required">*</i>
                              </div>
                            </div>
                        
                            <div className="col-lg-12">
                              <span className="pf-title">Categories</span>
                              <div className="pf-field no-margin">
                                <Select
                                  isMulti
                                  isClearable
                                  value={this.state.categories || null}
                                  onChange={event =>
                                    this.chosenHandleChange(event, "categories")
                                  }
                                  options={CategoryTags}
                                />
                                <i className="required">*</i>

                              
                              </div>
                            </div>
                            <div className="col-lg-12">
                              <span className="pf-title">Description</span>
                              <div className="pf-field">
                                <textarea
                                  name="description"
                                  onChange={this.handleChange}
                                  value={this.state.description}
                                  placeholder="Description with at least 300 characters"
                                />
                                <i className="required">*</i>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div className="social-edit">
                        <h3>Social Edit</h3>
                        <form>
                          <div className="row">
                            <div className="col-lg-6">
                              <span className="pf-title">Facebook</span>
                              <div className="pf-field">
                                <input
                                  type="text"
                                  name="facebook"
                                  value={this.state.facebook}
                                  onChange={this.handleChange}
                                />
                                <i className="fa fa-facebook" />
                              </div>
                            </div>
                            <div className="col-lg-6">
                              <span className="pf-title">Twitter</span>
                              <div className="pf-field">
                                <input
                                  type="text"
                                  name="twitter"
                                  value={this.state.twitter}
                                  onChange={this.handleChange}
                                />
                                <i className="fa fa-twitter" />
                              </div>
                            </div>
                            <div className="col-lg-6">
                              <span className="pf-title">Instagram</span>
                              <div className="pf-field">
                                <input
                                  type="text"
                                  name="instagram"
                                  value={this.state.instagram}
                                  onChange={this.handleChange}
                                />
                                <i className="la la-instagram" />
                              </div>
                            </div>
                            <div className="col-lg-6">
                              <span className="pf-title">Linkedin</span>
                              <div className="pf-field">
                                <input
                                  type="text"
                                  name="linkedin"
                                  value={this.state.linkedin}
                                  onChange={this.handleChange}
                                />
                                <i className="la la-linkedin" />
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div className="contact-edit">
                        <h3>Contact</h3>
                        <form>
                          <div className="row">
                            <div className="col-lg-4">
                              <span className="pf-title">Phone Number</span>
                              <div className="pf-field">
                                <input
                                  name="telephoneNumber"
                                  type="text"
                                  value={this.state.telephoneNumber}
                                  onChange={this.handleChange}
                                  placeholder="+90 538 963 58 96"
                                />
                              </div>
                            </div>
                            <div className="col-lg-4">
                              <span className="pf-title">Email</span>
                              <div className="pf-field">
                                <input
                                  type="text"
                                  name="email"
                                  value={this.state.email}
                                  disabled
                                  placeholder="artist@artist.com"
                                />
                              </div>
                            </div>
                            <div className="col-lg-4">
                              <span className="pf-title">Website</span>
                              <div className="pf-field">
                                <input
                                  type="text"
                                  name="website"
                                  value={this.state.website}
                                  onChange={this.handleChange}
                                  placeholder="www.artist.com"
                                />
                              </div>
                            </div>
                            <div className="col-lg-6">
                              <span className="pf-title">Country</span>
                              <div className="pf-field">
                                <Select
                                  className="chosen-container"
                                  classNamePrefix="chosen"
                                  value={this.state.country || null}
                                  onChange={event =>
                                    this.chosenHandleChange(event, "country")
                                  }
                                  options={CountryOption}
                                />
                                <i className="required">*</i>
                              </div>
                            </div>
                            <div className="col-lg-6">
                              <span className="pf-title">City</span>
                              <div className="pf-field">
                                <Select
                                  className="chosen-container"
                                  classNamePrefix="chosen"
                                  value={this.state.city || null}
                                  onChange={event =>
                                    this.chosenHandleChange(event, "city")
                                  }
                                  options={this.state.cityOptions}
                                />
                                <i className="required">*</i>
                              </div>
                            </div>
                           
                            <div className="col-lg-12">
                              <button
                                type="button"
                                onClick={this.updateProfile}
                              >
                                Update
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div className="videos-edit contact-edit">
                        <h3>Videos</h3>
                        <form>
                          <div className="row">
                            <div className="col-lg-4">
                              <span className="pf-title">Video Title </span>
                              <div className="pf-field">
                                <input
                                  name="videoTitle"
                                  type="text"
                                  value={this.state.artistVideos.videoTitle}
                                  onChange={event =>
                                    this.handleArtistGalChange(
                                      event,
                                      "videoTitle"
                                    )
                                  }
                                  placeholder="Video Title"
                                />
                              </div>
                            </div>
                            <div className="col-lg-4">
                              <span className="pf-title">Youtube URL</span>
                              <div className="pf-field">
                                <input
                                  name="videoUrl"
                                  type="text"
                                  value={this.state.artistVideos.videoUrl}
                                  onChange={event =>
                                    this.handleArtistGalChange(
                                      event,
                                      "videoUrl"
                                    )
                                  }
                                  placeholder="https://youtube.com/..."
                                />
                              </div>
                            </div>
                            <div className="col-lg-12">
                              <button type="button" onClick={this.updateVideos}>
                                Update Video
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div className="gallery-edit contact-edit">
                        <h3>Gallery</h3>
                        <form>
                          <div className="row">
                            <div className="upload-info">
                         

                              <ImageUploader
                                withIcon={true}
                                withPreview={true}
                                buttonText="Choose images"
                                onChange={this.onDrop}
                                imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                                maxFileSize={5242880}
                              />
                            </div>
                            <div className="col-lg-12">
                              <button
                                type="button"
                                onClick={this.updateGallery}
                              >
                                Update Gallery
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div className="soundcloud-edit contact-edit">
                        <h3>SoundCloud</h3>
                        <form>
                          <div className="row">
                            <div className="col-lg-4">
                              <span className="pf-title">Tracklist Title </span>
                              <div className="pf-field">
                                <input
                                  name="soundCloudTitle"
                                  type="text"
                                  value={
                                    this.state.artistSoundCloud.soundCloudTitle
                                  }
                                  onChange={event =>
                                    this.handleArtistSCChange(
                                      event,
                                      "soundCloudTitle"
                                    )
                                  }
                                  placeholder="Tracklist Title"
                                />
                              </div>
                            </div>
                            <div className="col-lg-4">
                              <span className="pf-title">
                                SoundCloud Embed URL
                              </span>
                              <div className="pf-field">
                                <input
                                  name="soundCloudUrl"
                                  type="text"
                                  value={
                                    this.state.artistSoundCloud.soundCloudUrl
                                  }
                                  onChange={event =>
                                    this.handleArtistSCChange(
                                      event,
                                      "soundCloudUrl"
                                    )
                                  }
                                  placeholder="<iframe..."
                                />
                              </div>
                            </div>
                            <div className="col-lg-12">
                              <button
                                type="button"
                                onClick={this.updateSoundCloud}
                              >
                                Update Sound Cloud
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <Footer />
        </div>
        <SideProfile />
        <Loader loading={this.state.loading} />
      </CSSTransitionGroup>
    )
  }
}

function mapStateToProps({ app }) {
  return {
    isLoggedIn: app.isLoggedIn,
    userid: app.userid
  }
}

export default connect(
  mapStateToProps,
  { push, setImage, setNameOnly, setRedirect }
)(EditProfile)
