import React, { Component } from "react"
import { connect } from "react-redux"
import CSSTransitionGroup from "react-addons-css-transition-group"
import { push } from "react-router-redux"
import load from "../jqueryscripts/profile"
import onLoad from "../jqueryscripts/onLoad"
import ResponsiveHeader from "../layout/ResponsiveHeader"
import Footer from "../layout/Footer"
import Header from "../layout/Header"
import moment from "moment"
import { Link } from "react-router-dom"
const GenreTags = [
  { value: 1, label: "Goth" },
  { value: 2, label: "Classic" },
  { value: 3, label: "Rnb" },
  { value: 4, label: "Melow" },
  { value: 5, label: "Hip Hop" }
]
class Event extends Component {
  state = {
    eventLocation: "",
    eventLength: "",
    eventName: "",
    eventDescription: "",
    eventPrice: "",
    eventCreated: "",
    eventGenre:"",
    eventEmail:""
  }
  componentDidMount() {
    const { id } = this.props.match.params
    if (id) {
      
    }
  }

  componentWillReceiveProps() {
    load()
    onLoad()
  }

  render() {
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div className="theme-layout" id="scrollup">
          <ResponsiveHeader />
          <Header />
          <section className="overlape">
            <div className="block no-padding">
              <div
                data-velocity="-.1"
                style={{
                  background:
                    "url(/images/bg_header_artist.jpg) repeat scroll 50% 422.28px transparent"
                }}
                className="parallax scrolly-invisible no-parallax"
              />
              <div className="container fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="inner-header">
                      <h3>{this.state.eventName || "Event"}</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="block">
              <div className="container">
                <div className="row">
                  <div className="col-lg-8 column">
                    <div className="job-single-sec">
                      <div className="job-single-head">
                        <div className="job-statistic">
                          <p>
                            <i className="la la-map-marker" />
                            {this.state.eventLocation || "TBA"}
                          </p>
                          <p>
                            <i className="la la-calendar-o" /> Posted{" "}
                            {this.state.eventCreated
                              ? moment(this.state.eventCreated).format(
                                  "MMM D, YYYY"
                                )
                              : "TBA"}
                          </p>
                        </div>
                      </div>
                      <div className="job-details">
                        <h3>Job Description</h3>
                        <p>{this.state.eventDescription || "TBA"}</p>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 column">
                    {this.props.isLoggedIn ? 
                      this.props.isArtist  ?
                      <a href={`mailto:${this.state.eventEmail}`} className="apply-thisjob" title="">
                        <i className="la la-paper-plane" />
                        Apply for this gig
                      </a>
                      :""
                     : (
                      <Link to="/register" className="apply-thisjob" title="">
                        Sign up to apply
                      </Link>
                    )}
                    <div className="job-overview gig">
                      <h3>Gig Overview</h3>
                      <ul>
                        <li>
                          <i className="la la-thumb-tack" />
                          <h3>Genre</h3>
                          <span>
                            { this.state.eventGenre && this.state.eventGenre.split(",").map(genre=> GenreTags.find(g => g.value === genre ).label).join(",").replace(",",", ")}
                          </span>
                        </li>
                        <li>
                          <i className="la la-money" />
                          <h3>Paid Gig</h3>
                          <span>{`£${this.state.eventPrice}` || "TBA"}</span>
                        </li>
                        <li>
                          <i className="la la-mars-double" />
                          <h3>Performance Length</h3>
                          <span>
                            {` ${this.state.eventLength} Minutes` || "TBA"}{" "}
                          </span>
                        </li>
                      </ul>
                    </div>
                    {this.state.eventLocation && (
                      <div className="job-location">
                        <h3>Job Location</h3>
                        <div className="job-lctn-map">
                          <iframe
                            title="Google map"
                            src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyAMdZuH62NXIOlKuoodYfwkY5jKntAX7wI&q=${
                              this.state.eventLocation
                            }`}
                          />
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </section>

          <Footer />
        </div>
      </CSSTransitionGroup>
    )
  }
}

function mapStateToProps({ app }) {
  return {
    isLoggedIn: app.isLoggedIn,
    userid: app.userid,
    isArtist: app.isArtist
  }
}

export default connect(
  mapStateToProps,
  { push }
)(Event)
