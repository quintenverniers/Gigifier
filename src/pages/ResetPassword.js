import React, { Component } from "react"
import CSSTransitionGroup from "react-addons-css-transition-group"
import Footer from "../layout/Footer"
import ResponsiveHeader from "../layout/ResponsiveHeader"
import Header from "../layout/Header"
import { toast } from "react-toastify"
import qs from "qs"

class ResetPassword extends Component {
  state = {
    newpassword: "",
    verifypassword: "",
    loading: false
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  submit = () => {
    const { newpassword, verifypassword } = this.state
  }

  render() {
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div className="theme-layout" id="scrollup">
          <ResponsiveHeader />
          <Header type="stick-top" />
          <section className="overlape">
            <div className="block no-padding">
              <div
                data-velocity="-.1"
                style={{
                  background:
                    "url(images/bg_header_artist.jpg) repeat scroll 50% 422.28px transparent"
                }}
                className="parallax scrolly-invisible no-parallax"
              />
              <div className="container fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="inner-header">
                      <h3>Welcome</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="block no-padding">
              <div className="container">
                <div className="row no-gape">
                  <div className="col-lg-9 column">
                    <div className="padding-left">
                      <div className="manage-jobs-sec">
                        <h3>Change Password</h3>
                        <div className="change-password">
                          <form>
                            <div className="row">
                              <div className="col-lg-6">
                                <span className="pf-title">New Password</span>
                                <div className="pf-field">
                                  <input
                                    type="password"
                                    name="newpassword"
                                    onChange={this.handleChange}
                                    value={this.state.newpassword}
                                  />
                                </div>
                                <span className="pf-title">
                                  Confirm Password
                                </span>
                                <div className="pf-field">
                                  <input
                                    type="password"
                                    name="verifypassword"
                                    onChange={this.handleChange}
                                    value={this.state.verifypassword}
                                  />
                                </div>
                                <button type="button" onClick={this.submit}>
                                  Reset Password
                                </button>
                              </div>
                              <div className="col-lg-6">
                                <i className="la la-key big-icon" />
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <Footer />
        </div>
      </CSSTransitionGroup>
    )
  }
}

export default ResetPassword
