import { Component } from "react";
import { setAuth, getParameterByName } from "../helper";

class Callback extends Component {


  componentDidMount() {
    setAuth({
      token: getParameterByName("token"),
      userid: getParameterByName("userId")
    });
    window.location.replace("/");
  }

  render() {
    return null;
  }
}

export default Callback;
