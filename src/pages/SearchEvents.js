import React, { Component } from "react"
import { connect } from "react-redux"
import { push } from "react-router-redux"
import CSSTransitionGroup from "react-addons-css-transition-group"
import Footer from "../layout/Footer"
import Header from "../layout/Header"
import ResponsiveHeader from "../layout/ResponsiveHeader"
import EventItem from "../components/Events/EventItem"
import qs from "qs"
import Pagination from 'rc-pagination'
import 'rc-pagination/assets/index.css';

class SearchEvents extends Component {
  state = {
    keyword: "",
    location: "",
    sort: "",
    events: [],
    current:1
  }

  componentDidMount() {
    
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.isLoggedIn) {
      this.props.push("/")
    }
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })

    if (event.target.value === "") {
      
    }
  }

  handleSelect = event => {
    this.setState({ location: event.value })
  }

  search = e => {
  }

  onChangee = (page) => {
    this.setState({
      current: page,
    });
  }

  render() {
    const events = this.state.events.slice((this.state.current -1) * 10, this.state.current * 10);

    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <ResponsiveHeader />
        <Header />

        <section className="overlape">
          <div className="block no-padding">
            <div
              data-velocity="-.1"
              style={{
                background:
                  "url(/images/bg_header_artist.jpg) repeat scroll 50% 422.28px transparent"
              }}
              className="parallax scrolly-invisible no-parallax"
            />
            <div className="container fluid">
              <div className="row">
                <div className="col-lg-12">
                  <div className="inner-header wform">
                    <div className="job-search-sec">
                      <div className="job-search">
                        <h4>Find events suited for your talent</h4>
                        <form onSubmit={e => e.preventDefault()}>
                          <div className="row">
                            <div className="col-lg-11">
                              <div className="job-field">
                                <input
                                  type="text"
                                  name="keyword"
                                  placeholder="Location... (e.g Belgium)"
                                  value={this.state.keyword}
                                  onKeyPress={this.search}
                                  onChange={this.handleChange}
                                />
                                <i className="la la-keyboard-o" />
                              </div>
                            </div>
        
                            <div className="col-lg-1">
                              <button type="button">
                                <i
                                  id="search"
                                  className="la la-search"
                                  onClick={this.search}
                                />
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="block">
            <div className="container">
              <div className="row">
                <div className="col-lg-12">
              
                  <div className="job-grid-sec">
                    <div className="row">
                      {this.state.events.length > 0 ? (
                        this.state.events.map(event => (
                          <EventItem
                            key={event.id}
                            id={event.id}
                            title={event.eventName}
                            location={event.eventLocation}
                            email={event.eventEmail}
                          />
                        ))
                      ) : (
                        <div>No Events</div>
                      )}
                    </div>
                  </div>
                       {events.length > 0 && <Pagination onChange={this.onChangee} current={this.state.current} total={this.state.events.length}  /> }
                </div>
              </div>
            </div>
          </div>
        </section>

        <Footer />
      </CSSTransitionGroup>
    )
  }
}

function mapStateToProps({ app }) {
  return {
    isLoggedIn: app.isLoggedIn
  }
}

export default connect(
  mapStateToProps,
  { push }
)(SearchEvents)
