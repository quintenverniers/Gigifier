import React, { Component } from "react"
import { connect } from "react-redux"
//import { isAuthenticated, getAuth } from "../helper"
import { push } from "react-router-redux"
import { Link } from "react-router-dom"
import CSSTransitionGroup from "react-addons-css-transition-group"
import load from "../jqueryscripts/main"
import onLoad from "../jqueryscripts/onLoad"
//import parallax from "../jqueryscripts/parallax"
import Footer from "../layout/Footer"
import Header from "../layout/Header"
import ResponsiveHeader from "../layout/ResponsiveHeader"
import Slider from "react-slick"
import Select from "react-select"

const SearchType = [
  { value: "artist", label: "Looking for an Artist" },
  {
    value: "gig",
    label: "Looking for a Gig"
  }
]

const SearchLocation = [{ value: "Belgium", label: "Belgium" }]
const SearchGig = [
  { value: "For Event", label: "For Event" },
  { value: "For Collaboration", label: "For Collaboration" }
]

const SearchArtist = [
  { value: "Musician", label: "Musician" },
  { value: "Comedian", label: "Comedian" },
  { value: "Actor", label: "Actor" },
  { value: "Painter", label: "Painter" },
  { value: "Writer", label: "Writer" },
  { value: "Other", label: "Other" }
]

class Main extends Component {
  state = {
    keyword: "",
    location: "",
    searchType: SearchType[0]
  }

  componentDidMount() {
    
  }

  componentWillReceiveProps() {
    load()
    onLoad()
  }

  chosenHandleChange = (event, name) => {
    this.setState({
      [name]: event
    })
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  search = () => {
    const { keyword, location, searchType } = this.state
    const queryLocation = location ? `location=${location.value}` : ""
    const queryString = queryLocation
    if (searchType.value === "artist") {
      this.props.push(`/search/artists?${queryString}`)
    } else {
      this.props.push(`/search/events?${queryString}`)
    }
  }

  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    }
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div className="theme-layout" id="scrollup">
          <ResponsiveHeader />
          <Header />

          <section>
            <div className="block no-padding">
              <div className="container fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="main-featured-sec">
                      <ul className="main-slider-sec text-arrows">
                        <li className="slideHome">
                          <img src="/images/header.png" alt="" />
                        </li>
                        <li className="slideHome">
                          <img src="/images/header2.jpg" alt="" />
                        </li>
                        <li className="slideHome">
                          <img src="/images/header3.jpg" alt="" />
                        </li>
                      </ul>
                      <div className="job-search-sec">
                        <div className="job-search">
                          <h3>
                            Easiest way to find the right
                            <br /> talent for your event
                          </h3>
                          <form>
                            <div className="row">
                              <div className="col-lg-4 col-md-2 col-sm-4 col-xs-12">
                                <div className="job-field">
                                  <Select
                                    className="chosen-container"
                                    classNamePrefix="chosen"
                                    value={this.state.searchType}
                                    onChange={event =>
                                      this.chosenHandleChange(
                                        event,
                                        "searchType"
                                      )
                                    }
                                    options={SearchType}
                                  />
                                </div>
                              </div>

                              <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div className="job-field">
                                  {this.state.searchType.value ===
                                  SearchType[0].value ? (
                                    <Select
                                      className="chosen-container"
                                      classNamePrefix="chosen"
                                      placeholder="I am a (e.g musician, artist ..."
                                      value={this.state.keyword}
                                      onChange={event =>
                                        this.chosenHandleChange(
                                          event,
                                          "keyword"
                                        )
                                      }
                                      options={SearchArtist}
                                    />
                                  ) : (
                                    <Select
                                      className="chosen-container"
                                      classNamePrefix="chosen"
                                      placeholder="For an event/For a collaboration"
                                      value={this.state.keyword}
                                      onChange={event =>
                                        this.chosenHandleChange(
                                          event,
                                          "keyword"
                                        )
                                      }
                                      options={SearchGig}
                                    />
                                  )}
                                
                                </div>
                              </div>

                              <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                <div className="job-field">
                                  <Select
                                    className="chosen-container"
                                    classNamePrefix="chosen"
                                    placeholder="Location"
                                    value={this.state.location}
                                    onChange={event =>
                                      this.chosenHandleChange(event, "location")
                                    }
                                    options={SearchLocation}
                                  />
                                 
                                </div>
                              </div>
                              <div className="col-lg-1 col-md-2 col-sm-2 col-xs-12">
                                <button type="button" onClick={this.search}>
                                  <i className="la la-search" />
                                </button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section id="scroll-here">
            <div className="block">
              <div className="container">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="heading">
                      <h2>How it Works</h2>
                      <span>Learn how our site works and sign up now!</span>
                    </div>
                    <div className="how-to-sec">
                      <div className="how-to">
                        <span className="how-icon">
                          <i className="la la-user" />
                        </span>
                        <h3>Browse / Create</h3>
                        <p>
                          Gigifier helps you getting your creations out of your
                          bedroom and into the spotlight.
                        </p>
                      </div>
                      <div className="how-to">
                        <span className="how-icon">
                          <i className="la la-file-archive-o" />
                        </span>
                        <h3>Apply / Hire</h3>
                        <p>
                          Find upcoming talent close to you and hire them or
                          just find out where they perform.
                        </p>
                      </div>
                      <div className="how-to">
                        <span className="how-icon">
                          <i className="la la-list" />
                        </span>
                        <h3>Enjoy</h3>
                        <p>
                          If you are just here to browse, enjoy all the local
                          and international talent.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="block double-gap-top double-gap-bottom">
              <div
                data-velocity="-.1"
                style={{
                  background:
                    "url(images/parallax_bg.png) repeat scroll 50% 422.28px transparent"
                }}
                className="parallax scrolly-invisible"
              />
              <div className="container">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="simple-text-block">
                      <h3>
                        Let venues and <br /> promoters find you
                      </h3>
                      <span>
                        The Gigifier browse tool lets venue bookers, promoters,
                        and private hosts search for <br />
                        musicians. Sign up and let venues and promoters find
                        you.
                      </span>
                      <Link to="/register" title="Register">
                        Create an Account
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="block">
              <div className="container">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="heading">
                      <h2>What Our Users Say</h2>
                      <span>
                        What other people thought about the service provided by
                        JobHunt
                      </span>
                    </div>
                    <div className="reviews-sec" id="reviews">
                      <Slider {...settings}>
                        <div className="col-lg-12">
                          <div className="reviews style2">
                            <img src="https://placehold.it/101x101" alt="" />
                            <h3>
                              Juan Tamad <span>Organ Player</span>
                            </h3>
                            <p>
                              Bulputate volutpat, eros pede semper est, vitae
                              luctus metus libero eu augue. Morbi purus libero!
                            </p>
                          </div>
                        </div>
                        <div className="col-lg-12">
                          <div className="reviews style2">
                            <img src="https://placehold.it/101x101" alt="" />
                            <h3>
                              Jane Doe <span>Band</span>
                            </h3>
                            <p>
                              Vestibulum volutpat, lacus a ultrices sagittis, mi
                              neque euismod dui, eu pulvinar nunc sapien.{" "}
                            </p>
                          </div>
                        </div>
                        <div className="col-lg-12">
                          <div className="reviews style2">
                            <img src="https://placehold.it/101x101" alt="" />
                            <h3>
                              Juan dela Cruz <span>Event Organizer</span>
                            </h3>
                            <p>
                              Commodo quis, gravida id, est. Sed lectus.
                              Praesent elementum hendrerit tortor. Sed semper
                              lorem at felis.{" "}
                            </p>
                          </div>
                        </div>
                        <div className="col-lg-12">
                          <div className="reviews style2">
                            <img src="https://placehold.it/101x101" alt="" />
                            <h3>
                              Mayumi Ochoa <span>Solo Artist</span>
                            </h3>
                            <p>
                              Sed egestas, ante et vulputate volutpat, eros pede
                              semper est, vitae luctus metus libero eu augue.
                              Morbi purus libero.
                            </p>
                          </div>
                        </div>
                      </Slider>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="block no-padding">
              <div className="container fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="simple-text">
                      <h3>Got a question?</h3>
                      <span>
                      We are here to help you. If you have any questions or need further help <br /> developing your profile contact us on the following address: our.gig.project@gmail.com
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <Footer />
        </div>
      </CSSTransitionGroup>
    )
  }
}

function mapStateToProps({ app }) {
  return {
    isLoggedIn: app.isLoggedIn,
    userid: app.userid
  }
}
export default connect(
  mapStateToProps,
  { push }
)(Main)
