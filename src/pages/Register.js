import React, { Component } from "react"
import { connect } from "react-redux"
import CSSTransitionGroup from "react-addons-css-transition-group"
import Footer from "../layout/Footer"
import Header from "../layout/Header"
import ResponsiveHeader from "../layout/ResponsiveHeader"
import { Link } from "react-router-dom"
import { performRegister } from "../actions/register"
import { ToastContainer, toast } from "react-toastify"
import Select from "react-select"
import Loader from "../components/Loader"
import FacebookLogin from "react-facebook-login"

const ArtistTypes = [
  { value: "solo", label: "Solo Artist" },
  {
    value: "band",
    label: "Band"
  },
  {
    value: "instrument",
    label: "Instrument Player"
  },
  { value: "orchestra", label: "Orchestra" }
]

class Register extends Component {
  state = {
    successfull: null,
    username: "",
    password: "",
    confirmPassword: "",
    email: "",
    confirmEmail: "",
    userType: "artist",
    artistType: ArtistTypes[0],
    name: "",
    loading: false,
    error: false,
    message: ""
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  selectType = userType => {
    this.setState({ userType })
  }

  chosenHandleChange = (event, name) => {
    this.setState({
      [name]: event
    })
  }

  notify = (errMessage) => toast.error(errMessage);

  submit = () => {
    console.log("Username: ",this.state.username,"\nPaswoord: ",this.state.password,"\nEmail: ",this.state.email,"\nUser Type: ",this.state.userType,"\nArtist Type: ",this.state.artistType.value,"\nName: ",this.state.name);
   const artistType =
      this.state.userType === "artist" ? this.state.artistType : null
    
    const password = this.state.password.trim()

    if (password.length < 8) {
      this.setState({
        error: true,
        message: "Password must have a minimum of 8 characters"
      })
      this.notify("Password must have a minimum of 8 characterrs");
      return
    }

    if (!/\d/.test(password)) {
      this.setState({ error: true, message: "Password must contain a number" })
      this.notify("Password must contain a number");
      return
    }

    if(this.state.password !== this.state.confirmPassword){
      this.setState({ error: true, message: "Passwords do not match" })
      this.notify("Passwords do not match");
      return
    }

    if(this.state.email !== this.state.confirmEmail){
      this.setState({ error: true, message: "Emails do not match" })
      this.notify("Emails do not match");
      return
    }

    //this.setState({ loading: true })
    //Do registration
    //this.setState({ Loading: false })
  }

  render() {
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div className="theme-layout" id="scrollup">
          <ResponsiveHeader />
          <Header type="gradient" />
          <section>
            <div className="block no-padding  gray">
              <div className="container">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="inner2">
                      <div className="inner-title2">
                        <h3>Register</h3>
                        <span>Thank you for registering</span>
                      </div>
                      <div className="page-breacrumbs">
                        <ul className="breadcrumbs">
                          <li>
                            <Link to="/" title="Home">
                              Home
                            </Link>
                          </li>
                          <li>
                            <Link to="/" title="Pages">
                              Pages
                            </Link>
                          </li>
                          <li>
                            <Link to="/register" title="Register">
                              Register
                            </Link>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="block remove-bottom">
              <div className="container">
                <div className="row">
                  {this.state.successfull ? (
                    <div className="col-lg-12">
                      <p>A message was sent to your email</p>
                      <p>Please verify your email before logging in.</p>
                    </div>
                  ) : (
                    <div className="col-lg-12">
                      <div className="account-popup-area signup-popup-box static">
                        <div className="account-popup">
                          <h3>Sign Up</h3>
                          <span>
                            Lorem ipsum dolor sit amet consectetur adipiscing
                            elit odio duis risus at lobortis ullamcorper
                          </span>
                          <div className="select-user">
                            <span
                              style={{
                                borderColor:
                                  this.state.userType === "artist"
                                    ? "#fb236a"
                                    : null
                              }}
                              onClick={() => this.selectType("artist")}
                            >
                              Artist
                            </span>
                            <span
                              style={{
                                borderColor:
                                  this.state.userType === "organizer"
                                    ? "#fb236a"
                                    : null
                              }}
                              onClick={() => this.selectType("organizer")}
                            >
                              Hire an artist
                            </span>
                          </div>
                          <form>
                            <div className="cfield">
                              <input
                                type="text"
                                name="username"
                                placeholder="ArtistName"
                                value={this.state.username}
                                onChange={this.handleChange}
                              />
                              <i className="la la-user" />
                            </div>
                            <div className="cfield">
                              <input
                                type="password"
                                name="password"
                                placeholder="Password (e.g qwerty12)"
                                value={this.state.password}
                                onChange={this.handleChange}
                              />
                              <i className="la la-key" />
                            </div>
                            <div className="cfield">
                              <input
                                type="password"
                                name="confirmPassword"
                                placeholder="Confirm Password (e.g qwerty12)"
                                value={this.state.confirmPassword}
                                onChange={this.handleChange}
                              />
                              <i className="la la-key" />
                            </div>
                            <div className="cfield">
                              <input
                                type="text"
                                name="email"
                                placeholder="Email"
                                value={this.state.email}
                                onChange={this.handleChange}
                              />
                              <i className="la la-envelope-o" />
                            </div>
                            <div className="cfield">
                              <input
                                type="text"
                                name="confirmEmail"
                                placeholder="Confirm Email"
                                value={this.state.confirmEmail}
                                onChange={this.handleChange}
                              />
                              <i className="la la-envelope-o" />
                            </div>
                            <div className="cfield">
                              <input
                                type="text"
                                name="name"
                                placeholder="Name"
                                value={this.state.name}
                                onChange={this.handleChange}
                              />
                              <i className="la la-user" />
                            </div>
                            {this.state.userType === "artist" && (
                              <div className="dropdown-field">
                                <Select
                                  className="chosen-container"
                                  classNamePrefix="chosen"
                                  placeholder="Artist Type"
                                  value={this.state.artistType}
                                  onChange={event =>
                                    this.chosenHandleChange(event, "artistType")
                                  }
                                  options={ArtistTypes}
                                />
                              </div>
                            )}
                          
                            {/*this.state.error && (
                              <div>{this.state.message}</div>
                            )*/}
                            <button type="button" onClick={this.submit}>
                              Signup
                            </button>
                          </form>
                          <div className="extra-login">
                            <span>Or</span>
                            <div className="login-social">
                             
                              <FacebookLogin
                                appId="1373362946099639"
                                fields="name,email"
                                // onClick={componentClicked}
                                cssClass="fa fa-facebook"
                                callback={this.responseFacebook}
                                textButton=""
                                tag="a"
                                typeButton=""
                                redirectUri="https://gigifier.com/register"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </section>
          <Footer showScroll={false}/>
        </div>
        <Loader loading={this.state.loading} />
      </CSSTransitionGroup>
    )
  }
}

export default connect(
  null,
  { performRegister }
)(Register)
