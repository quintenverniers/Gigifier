import React, { Component } from "react"
import { connect } from "react-redux"
import { push } from "react-router-redux"
import CSSTransitionGroup from "react-addons-css-transition-group"
import Footer from "../layout/Footer"
import Header from "../layout/Header"
import Select from "react-select"
import SideProfile from "../layout/SideProfile"
import Sidebar from "../layout/Sidebar"
import ResponsiveHeader from "../layout/ResponsiveHeader"
import { getCountries, getCities } from "../helper"
import NumberFormat from "react-number-format"
import { toast } from "react-toastify"
import { setImage } from "../actions"
import Loader from "../components/Loader"

const CountryOption = getCountries()
const GenreTags = [
  { value: 1, label: "Goth" },
  { value: 2, label: "Classic" },
  { value: 3, label: "Rnb" },
  { value: 4, label: "Melow" },
  { value: 5, label: "Hip Hop" }
]
class PostJob extends Component {
  state = {
    eventName: "",
    eventLocation: "",
    eventDescription: "",
    eventCountry: null,
    eventPrice: "",
    eventLength: "",
    eventCity: null,
    eventGenre: null,
    loading: false,
    cityOptions: [],
    eventEmail: ""
  }
  componentDidMount() {
    const { userid } = this.props
  }
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  chosenHandleChange = (event, name) => {
    this.setState({
      [name]: event
    })
  }
  submit = () => {
    const data = {
      ...(this.state.eventPrice && { eventPrice: this.state.eventPrice }),
      ...(this.state.eventCity && { eventCity: this.state.eventCity.value }),
      ...(this.state.eventLength && { eventLength: this.state.eventLength }),
      ...(this.state.eventName && { eventName: this.state.eventName }),
      ...(this.state.eventLocation && {
        eventLocation: this.state.eventLocation
      }),
      ...(this.state.eventCountry && {
        eventCountry: this.state.eventCountry.value
      }),
      ...(this.state.eventDescription && {
        eventDescription: this.state.eventDescription
      }),
      ...(this.state.eventGenre && {
        eventGenre: this.state.eventGenre.map(genre => genre.value)
      }),
      eventCreated: new Date(),
      eventOwner: this.props.userid,
      eventEmail: this.state.eventEmail
    }
    if (
      !this.state.eventPrice ||
      !this.state.eventCity ||
      !this.state.eventCountry ||
      !this.state.eventGenre ||
      !this.state.eventLength ||
      !this.state.eventLocation ||
      !this.state.eventName ||
      !this.state.eventDescription
    ) {
      toast.error("Please fill up required fields.")
      return
    }
    this.setState({ loading: true })

  }
  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.eventCountry !== this.state.eventCountry &&
      this.state.eventCountry != null
    ) {
      this.setState({ cityOptions: getCities(this.state.eventCountry.label) })
    }
  }

  render() {
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div className="theme-layout" id="scrollup">
          <ResponsiveHeader />
          <Header />

          <section className="overlape">
            <div className="block no-padding">
              <div
                data-velocity="-.1"
                style={{
                  background:
                    "url(images/bg_header_artist.jpg) repeat scroll 50% 422.28px transparent"
                }}
                className="parallax scrolly-invisible no-parallax"
              />
              <div className="container fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="inner-header">
                      <h3>Post a New Job</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="block no-padding">
              <div className="container">
                <div className="row no-gape">
                  <Sidebar />
                  <div className="col-lg-9 column">
                    <div className="padding-left">
                      <div className="profile-title">
                        <h3>Post a New Job</h3>
                      </div>
                      <div className="profile-form-edit">
                        <form>
                          <div className="row">
                            <div className="col-lg-12">
                              <span className="pf-title">Job Title</span>
                              <div className="pf-field">
                                <input
                                  type="text"
                                  placeholder="Job Title"
                                  value={this.state.eventName}
                                  onChange={this.handleChange}
                                  name="eventName"
                                />
                                <i className="required">*</i>
                              </div>
                            </div>
                            <div className="col-lg-12">
                              <span className="pf-title">Description</span>
                              <div className="pf-field">
                                <textarea
                                  onChange={this.handleChange}
                                  name="eventDescription"
                                  value={this.state.eventDescription}
                                />
                                <i className="required">*</i>
                              </div>
                            </div>

                            <div className="col-lg-12">
                              <span className="pf-title">Genre</span>
                              <div className="pf-field no-margin">
                                <Select
                                  isMulti
                                  isClearable
                                  value={this.state.eventGenre || null}
                                  onChange={event =>
                                    this.chosenHandleChange(event, "eventGenre")
                                  }
                                  options={GenreTags}
                                />
                                <i className="required">*</i>
                              </div>
                            </div>
                            <div className="col-lg-6">
                              <span className="pf-title">Job Length (min)</span>
                              <div className="pf-field">
                                <NumberFormat
                                  value={this.state.eventLength}
                                  format="###"
                                  name="eventLength"
                                  onChange={this.handleChange}
                                  placeholder={60}
                                />
                                <i className="required">*</i>
                              </div>
                            </div>
                            <div className="col-lg-6">
                              <span className="pf-title">Job Price</span>
                              <div className="pf-field">
                                <NumberFormat
                                  name="eventPrice"
                                  thousandSeparator={true}
                                  fixedDecimalScale
                                  decimalScale={2}
                                  value={this.state.eventPrice}
                                  onChange={this.handleChange}
                                  placeholder={12345}
                                />
                                <i className="required">*</i>
                              </div>
                            </div>
                            <div className="col-lg-6">
                              <span className="pf-title">Country</span>
                              <div className="pf-field">
                                <Select
                                  className="chosen-container"
                                  classNamePrefix="chosen"
                                  value={this.state.eventCountry || null}
                                  onChange={event =>
                                    this.chosenHandleChange(
                                      event,
                                      "eventCountry"
                                    )
                                  }
                                  options={CountryOption}
                                />
                                <i className="required">*</i>
                              </div>
                            </div>
                            <div className="col-lg-6">
                              <span className="pf-title">City</span>
                              <div className="pf-field">
                                <Select
                                  className="chosen-container"
                                  classNamePrefix="chosen"
                                  value={this.state.eventCity || null}
                                  onChange={event =>
                                    this.chosenHandleChange(event, "eventCity")
                                  }
                                  options={this.state.cityOptions}
                                />
                                <i className="required">*</i>
                              </div>
                            </div>
                            <div className="col-lg-12">
                              <span className="pf-title">Complete Address</span>
                              <div className="pf-field">
                                <textarea
                                  name="eventLocation"
                                  value={this.state.eventLocation}
                                  onChange={this.handleChange}
                                />
                                <i className="required">*</i>
                              </div>
                            </div>
                          </div>
                          <button type="button" onClick={this.submit}>
                            Post Job
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <Footer />
        </div>
        <SideProfile />
        <Loader loading={this.state.loading} />
      </CSSTransitionGroup>
    )
  }
}

function mapStateToProps({ app }) {
  return {
    isLoggedIn: app.isLoggedIn,
    userid: app.userid
  }
}

export default connect(
  mapStateToProps,
  { push, setImage }
)(PostJob)
