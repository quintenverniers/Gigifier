import React, { Component } from "react"
import { connect } from "react-redux"
import { push } from "react-router-redux"
import CSSTransitionGroup from "react-addons-css-transition-group"
import Footer from "../layout/Footer"
import Header from "../layout/Header"
import ArtistsItem from "../components/Artists/ArtistItem"
import SpecialismItem from "../components/Artists/SpecialismItem"
import ResponsiveHeader from "../layout/ResponsiveHeader"
import { getLocationName } from "../helper"
import qs from "qs"
import GoogleMapReact from "google-map-react"
import _ from "lodash"
import Pagination from "rc-pagination"
import "rc-pagination/assets/index.css"

const AnyReactComponent = () => <img src="/images/marker1.png" alt="marker" />

class SearchArtists extends Component {
  state = {
    center: {
      lat: 53.385846,
      lng: 1.471385
    },
    keyword: "",
    location: "",
    artistType: [
      { id: "1", name: "Solo Artist", value: true, code: "solo" },
      { id: "2", name: "Band", value: true, code: "band" },
      { id: "3", name: "Instrument Player", value: true, code: "instrument" },
      { id: "4", name: "Orchestra", value: true, code: "orchestra" }
    ],
    age: [
      { id: "1", name: "12-22", value: true },
      { id: "2", name: "22-30", value: true },
      { id: "3", name: "30-40", value: true },
      { id: "4", name: "40-50", value: true }
    ],
    category: [
      { id: "1", name: "Guitar", value: true },
      { id: "2", name: "Drum", value: true },
      { id: "3", name: "Concert", value: true },
      { id: "4", name: "Acapela ", value: true },
      { id: "5", name: "Freestyle", value: true },
      { id: "6", name: "Musician", value: true },
      { id: "7", name: "Comedian", value: true },
      { id: "8", name: "Actor", value: true },
      { id: "9", name: "Theater ", value: true },
      { id: "10", name: "Painter", value: true },
      { id: "11", name: "Writer ", value: true },
      { id: "12", name: "Other", value: true },
    ],
    experience: [
      { id: "1", name: "0-2", value: true },
      { id: "2", name: "2-6", value: true },
      { id: "3", name: "6-12", value: true }
    ],
    current: 1,
    artists: [],
    filteredArtists: []
  }

  filterArtistType = artists => {
    const checkedArtistType = this.state.artistType.filter(type => type.value)
    return artists.filter(artist => {
      return _.some(checkedArtistType, { code: artist.artistType })
    })
  }

  onChangee = page => {
    this.setState({
      current: page
    })
  }
  componentDidMount() {
    const { location, keyword } = qs.parse(this.props.location.search, {
      ignoreQueryPrefix: true
    })
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.isLoggedIn) {
      this.props.push("/")
    }
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  handleArtistType = item => {
    this.setState(prevState => {
      return {
        artistType: prevState.artistType.map(type => {
          if (type.name === item) {
            return {
              ...type,
              value: !type.value
            }
          } else {
            return type
          }
        })
      }
    })
  }

  search = e => {
    if (e.key === "Enter" || e.target.id === "search") {
      const { keyword, location } = this.state
      const artists = _.filter(this.state.artists, data => {
        if (location === "" && keyword === "") {
          return true
        }

        if (keyword && location) {
          if (
            _.includes(data.name.toLowerCase(), keyword.toLowerCase()) &&
            _.includes(
              getLocationName(data.country, data.city).toLowerCase(),
              location.toLowerCase()
            )
          ) {
            return true
          } else {
            return false
          }
        } else {
          if (
            (keyword &&
              _.includes(data.name.toLowerCase(), keyword.toLowerCase())) ||
            (location &&
              _.includes(
                getLocationName(data.country, data.city).toLowerCase(),
                location.toLowerCase()
              ))
          ) {
            return true
          } else {
            return false
          }
        }
      })
      this.setState({ filteredArtists: artists })
    }
  }

  render() {
    const {
      keyword,
      artistType,
      filteredArtists,
      location,
      current
    } = this.state
    const artists = this.filterArtistType(filteredArtists).slice(
      (current - 1) * 10,
      current * 10
    )
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div className="theme-layout" id="scrollup">
          <ResponsiveHeader />
          <Header type="gradient" />

          <section>
            <div className="block no-padding">
              <div className="container fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div style={{ height: "40vh", width: "100%" }}>
                      <GoogleMapReact
                        bootstrapURLKeys={{
                          key: "AIzaSyAMdZuH62NXIOlKuoodYfwkY5jKntAX7wI"
                        }}
                        center={{
                          lat: 53.385846,
                          lng: 1.471385
                        }}
                        defaultZoom={2}
                      >
                        {artists.length > 0 &&
                          artists.map((artist, index) => {
                            if(!artist.lat){
                              return null
                            }
                            return (
                              <AnyReactComponent
                                key={index}
                                lat={artist.lat}
                                lng={artist.lng}
                              />
                            )
                          })}
                      </GoogleMapReact>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="block no-padding">
              <div className="container">
                <div className="row no-gape">
                  <aside className="col-lg-4 column border-right">
                    <div className="widget">
                      <div className="search_widget_job">
                        <div className="field_w_search">
                          <input
                            type="text"
                            placeholder="Search ..."
                            name="keyword"
                            value={keyword}
                            onChange={this.handleChange}
                            onKeyPress={this.search}
                          />
                          <i
                            id="search"
                            className="la la-search"
                            onClick={this.search}
                          />
                        </div>
                        <div className="field_w_search">
                          <input
                            type="text"
                            placeholder="All Locations"
                            name="location"
                            value={location}
                            onChange={this.handleChange}
                            onKeyPress={this.search}
                          />
                          <i className="la la-map-marker" />
                        </div>
                      </div>
                    </div>
                    <div className="widget">
                      <h3 className="sb-title open">Artist Type</h3>
                      <div className="specialism_widget">
                        <div className="simple-checkbox scrollbar">
                          {artistType.map(item => {
                            return (
                              <SpecialismItem
                                key={item.id}
                                checked={item.value}
                                name={item.name}
                                onClick={this.handleArtistType}
                              />
                            )
                          })}
                        </div>
                      </div>
                    </div>
                  </aside>
                  <div className="col-lg-8 column">
                    <div className="padding-left">
                      <div className="emply-resume-sec">
                        {artists.length > 0 ? (
                          artists.map(artist => {
                            return (
                              <ArtistsItem
                                key={artist.id}
                                id={artist.id}
                                name={artist.name}
                                image={artist.profPicUrl}
                                location={getLocationName(
                                  artist.country,
                                  artist.city
                                )}
                                type={artist.artistType}
                              />
                            )
                          })
                        ) : (
                          <div>No Artists</div>
                        )}
                        {artists.length > 0 && (
                          <Pagination
                            onChange={this.onChangee}
                            current={this.state.current}
                            total={filteredArtists.length}
                          />
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <Footer />
        </div>
      </CSSTransitionGroup>
    )
  }
}

function mapStateToProps(state) {
  const { app } = state
  return {
    isLoggedIn: app.isLoggedIn
  }
}

export default connect(
  mapStateToProps,
  { push }
)(SearchArtists)
