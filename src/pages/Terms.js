import React, { Component } from "react"
import CSSTransitionGroup from "react-addons-css-transition-group"
import Footer from "../layout/Footer"
import ResponsiveHeader from "../layout/ResponsiveHeader"
import Header from "../layout/Header"
import SideProfile from "../layout/SideProfile"
import { requestResetPassword } from "../services"
import { toast } from "react-toastify"

class ForgotPassword extends Component {
  render() {
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div className="theme-layout" id="scrollup">
          <ResponsiveHeader />
          <Header type="stick-top" />
          <section className="overlape">
            <div className="block no-padding">
              <div
                data-velocity="-.1"
                style={{
                  background:
                    "url(images/bg_header_artist.jpg) repeat scroll 50% 422.28px transparent"
                }}
                className="parallax scrolly-invisible no-parallax"
              />
              <div className="container fluid">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="inner-header">
                      <h3>Terms of Service</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div className="block no-padding">
              <div className="container">
                Gigifier Terms of Service 1. Terms By accessing the website at
                http://www.gigifier.com, you are agreeing to be bound by these
                terms of service, all applicable laws and regulations, and agree
                that you are responsible for compliance with any applicable
                local laws. If you do not agree with any of these terms, you are
                prohibited from using or accessing this site. The materials
                contained in this website are protected by applicable copyright
                and trademark law. 2. Use License a. Permission is granted to
                temporarily download one copy of the materials (information or
                software) on Gigifier's website for personal, non-commercial
                transitory viewing only. This is the grant of a license, not a
                transfer of title, and under this license you may not: i. modify
                or copy the materials; ii. use the materials for any commercial
                purpose, or for any public display (commercial or
                non-commercial); iii. attempt to decompile or reverse engineer
                any software contained on Gigifier's website; iv. remove any
                copyright or other proprietary notations from the materials; or
                v. transfer the materials to another person or "mirror" the
                materials on any other server. b. This license shall
                automatically terminate if you violate any of these restrictions
                and may be terminated by Gigifier at any time. Upon terminating
                your viewing of these materials or upon the termination of this
                license, you must destroy any downloaded materials in your
                possession whether in electronic or printed format. 3.
                Disclaimer a. The materials on Gigifier's website are provided
                on an 'as is' basis. Gigifier makes no warranties, expressed or
                implied, and hereby disclaims and negates all other warranties
                including, without limitation, implied warranties or conditions
                of merchantability, fitness for a particular purpose, or
                non-infringement of intellectual property or other violation of
                rights. b. Further, Gigifier does not warrant or make any
                representations concerning the accuracy, likely results, or
                reliability of the use of the materials on its website or
                otherwise relating to such materials or on any sites linked to
                this site. 4. Limitations In no event shall Gigifier or its
                suppliers be liable for any damages (including, without
                limitation, damages for loss of data or profit, or due to
                business interruption) arising out of the use or inability to
                use the materials on Gigifier's website, even if Gigifier or a
                Gigifier authorized representative has been notified orally or
                in writing of the possibility of such damage. Because some
                jurisdictions do not allow limitations on implied warranties, or
                limitations of liability for consequential or incidental
                damages, these limitations may not apply to you. 5. Accuracy of
                materials The materials appearing on Gigifier's website could
                include technical, typographical, or photographic errors.
                Gigifier does not warrant that any of the materials on its
                website are accurate, complete or current. Gigifier may make
                changes to the materials contained on its website at any time
                without notice. However Gigifier does not make any commitment to
                update the materials. 6. Links Gigifier has not reviewed all of
                the sites linked to its website and is not responsible for the
                contents of any such linked site. The inclusion of any link does
                not imply endorsement by Gigifier of the site. Use of any such
                linked website is at the user's own risk. 7. Modifications
                Gigifier may revise these terms of service for its website at
                any time without notice. By using this website you are agreeing
                to be bound by the then current version of these terms of
                service. 8. Governing Law These terms and conditions are
                governed by and construed in accordance with the laws of Belgium
                and you irrevocably submit to the exclusive jurisdiction of the
                courts in that State or location.
              </div>
            </div>
          </section>

          <Footer />
        </div>
        <SideProfile />
      </CSSTransitionGroup>
    )
  }
}

export default ForgotPassword
