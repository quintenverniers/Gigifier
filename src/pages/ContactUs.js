import React, { Component } from "react"
import CSSTransitionGroup from "react-addons-css-transition-group"
import Footer from "../layout/Footer"
import ResponsiveHeader from "../layout/ResponsiveHeader"
import Header from "../layout/Header"
import SideProfile from "../layout/SideProfile"
import { requestResetPassword } from "../services"
import { toast } from "react-toastify"

class ForgotPassword extends Component {
  state = {
    subject: "",
    body: "",
    to: "",
    name: ""
  }
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }
  render() {
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div className="theme-layout" id="scrollup">
          <ResponsiveHeader />
          <Header type="gradient" />
          <section>
            <div class="block no-padding  gray">
              <div class="container">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="inner2">
                      <div class="inner-title2">
                        <h3>Contact Us</h3>
                        <span>Send us your comments and suggestions below</span>
                      </div>
                      <div class="page-breacrumbs">
                        <ul class="breadcrumbs">
                          <li>
                            <a href="#" title="">
                              Home
                            </a>
                          </li>
                          <li>
                            <a href="#" title="">
                              Pages
                            </a>
                          </li>
                          <li>
                            <a href="#" title="">
                              Contact Us
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div class="block">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 column">
                    <div class="contact-form">
                      <h3>Keep In Touch</h3>
                      <form>
                        <div class="row">
                          <div class="col-lg-12">
                            <span class="pf-title">Subject</span>
                            <div class="pf-field">
                              <input
                                type="text"
                                value={this.state.subject}
                                onChange={this.handleChange}
                                name="subject"
                              />
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <span class="pf-title">Message</span>
                            <div class="pf-field">
                              <textarea
                                value={this.state.body}
                                onChange={this.handleChange}
                                name="body"
                              />
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <button>
                              <a
                                href={`mailto:our.gig.project@gmail.com?subject=${encodeURI(
                                  this.state.subject
                                )}&body=${encodeURI(this.state.body)}`}
                              >
                                Send
                              </a>
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="col-lg-6 column">
                    <div class="contact-textinfo">
                      <h3>Gigifier Team</h3>
                      <ul>
                        <li>
                          <i class="la la-map-marker" />
                          <span>Address Here </span>
                        </li>
                        <li>
                          <i class="la la-phone" />
                          <span>Call Us : 0934 343 343</span>
                        </li>
                        <li>
                          <i class="la la-fax" />
                          <span>Fax : 0934 343 343</span>
                        </li>
                        <li>
                          <i class="la la-envelope-o" />
                          <span>Email : our.gig.project@gmail.com</span>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <Footer />
        </div>
        <SideProfile />
      </CSSTransitionGroup>
    )
  }
}

export default ForgotPassword
